<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ComparePassword implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Сравнение нового пароля со старым
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return ! (\Hash::check($value, \Auth::user()->password));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Новый пароль не должен совпадать со старым.';
    }
}
