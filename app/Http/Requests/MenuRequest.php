<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ( $this->type === 'url' ) {
            return [
                'url'       => 'required|url',
                'url_title' => 'required|string',
            ];
        } elseif ( $this->type === 'page' ) {
            return [ 'page_id' => 'required|regex:/[^0]/u' ];
        } elseif ( $this->type === 'submenu' ) {
            return [ 'submenu_title' => 'required|string' ];
        }
    }
}
