<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
            return [
                'role'       => 'required|regex:/(user)?(admin)?/u',
                'first_name' => 'required|regex:/[А-Яа-яЁё]/u|max:60',
                'last_name'  => 'required|regex:/[А-Яа-яЁё]/u|max:60',
                'login'      => 'required|alpha_dash|max:255|unique:users,login,'
                                . $this->user->id,
                'email'      => 'required|email|max:255|unique:users,email,'
                                . $this->user->id,
            ];
        } else {
            return [
                'first_name' => 'required|regex:/[А-Яа-яЁё]/u|max:60',
                'last_name'  => 'required|regex:/[А-Яа-яЁё]/u|max:60',
                'login'      => 'required|alpha_dash|max:255|unique:users',
                'email'      => 'required|email|max:255|unique:users',
                'password'   => 'required|string|min:6|confirmed',
            ];
        }
    }
}
