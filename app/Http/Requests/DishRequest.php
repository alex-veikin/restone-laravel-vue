<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DishRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
            return [
                'cuisine_id'  => 'required',
                'category_id' => 'required',
                'title'       => 'required|string|max:255|unique:dishes,title,'
                    . $this->dish->id,
                'price'       => 'required|numeric',
                'file'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ];
        } else {
            return [
                'cuisine_id'  => 'required',
                'category_id' => 'required',
                'title'       => 'required|string|max:255|unique:dishes,title',
                'price'       => 'required|numeric',
                'file'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ];
        }
    }
}
