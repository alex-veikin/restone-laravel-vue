<?php

namespace App\Http\Controllers;

use App\Http\Requests\MenuRequest;
use App\Menu;
use App\MenuItem;
use App\Page;
use Illuminate\Http\Request;

class MenuController extends Controller
{

    /**
     * Отображение списка всех меню.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $menus = Menu::all();

        return view('admin.menus.index', compact('menus'));
    }

    /**
     * Добавление пункта меню.
     *
     * @param \App\Http\Requests\MenuRequest $request
     * @param                                $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(MenuRequest $request, $id)
    {
        // Получаем меню и колличество пунктов
        $menu  = Menu::find($id);
        $count = $menu->items()
            ->where('parent_id', $request->parent_id)
            ->count();

        // Данные пункта меню
        $data = [
            'type'          => $request->type,
            'page_id'       => $request->type === 'page'
                ? $request->page_id
                : null,
            'url'           => $request->type === 'url'
                ? $request->url
                : null,
            'url_title'     => $request->type === 'url'
                ? $request->url_title
                : null,
            'submenu_title' => $request->type === 'submenu'
                ? $request->submenu_title
                : null,
            'parent_id'     => $request->parent_id,
            'weight'        => ++$count,
        ];

        // Сохраняем пункт
        $menu->items()->create($data);

        return redirect()
            ->route('admin.menus.show', $menu->id)
            ->with("success", "Запись успешно создана!");
    }

    /**
     * Отображение пунктов выбранного меню
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        // Находим меню
        $menu = Menu::find($id)->load('items');

        // Формируем список всех страниц для формы
        $pages_select = ['-выберите страницу-'];
        foreach (Page::all() as $page) {
            $pages_select[$page->id] = $page->title;
        }

        // Формируем список всех подменю текущего меню для формы
        $parents_select = ['-нет-'];
        foreach ($menu->items->where('type', 'submenu') as $link) {
            $parents_select[$link->id] = $link->submenu_title;
        }

        return view('admin.menus.show',
            compact('menu', 'pages_select', 'parents_select'));
    }


    /**
     * Перемещение пункта меню вниз/вверх
     *
     * @param $id
     * @param $direction
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function move($id, $direction)
    {
        // Находим пункт
        $item = MenuItem::find($id);

        // Записываем новую позицию текущего пункта в переменную
        if ($direction === 'up') {
            $weight = $item->weight - 1;
        } elseif ($direction === 'down') {
            $weight = $item->weight + 1;
        }

        // Находим пункт для обмена позициями
        $item_exchange = MenuItem::where('menu_id', $item->menu_id)
            ->where('parent_id', $item->parent_id)
            ->where('weight', $weight)->first();

        // Меняем позицию пункта для обмена
        $item_exchange->update(['weight' => $item->weight]);

        // Меняем позицию текущего пункта
        $item->update(['weight' => $weight]);

        return redirect()
            ->back()
            ->with("success", "Запись успешно перемещена!");
    }


    /**
     * Удаление пункта меню
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        // Находим пункт
        $item = MenuItem::find($id);

        // Удаляем пункт и сдвигаем позиции нижележащих пунктов вверх
        if ($this->deleteMenuItems($item)) {
            $this->shiftMenuItems($item);

            return redirect()
                ->back()
                ->with("success", "Запись успешно удалена!");
        }
    }


    /**
     * Удаление пункта меню и всех его дочерних пунктов
     *
     * @param $item
     *
     * @return bool
     */
    private function deleteMenuItems($item)
    {
        // Находим дочерние пункты текущего пункта
        $children = MenuItem::where('menu_id', $item->menu_id)
            ->where('parent_id', $item->id)
            ->get();

        // Если есть дочерние пункты
        if ($children->count() > 0) {
            // Рекурсивно ищем в них дочерние пункты
            foreach ($children as $link) {
                $this->deleteMenuItems($link);

                // Удаляем дочерний пункт
                $link->delete();
            }
        }

        // Удаляем текущий пункт
        $item->delete();

        return true;
    }


    /**
     * Смещение номера пунктов меню после удаленного
     *
     * @param $item
     */
    private function shiftMenuItems($item)
    {
        // Находим пункты позиции которых ниже текущего
        $items = MenuItem::where('menu_id', $item->menu_id)
            ->where('parent_id', $item->parent_id)
            ->where('weight', '>', $item->weight)
            ->get();

        // Меняем позицию каждого пункта на единицу вверх
        foreach ($items as $link) {
            $weight = $link->weight - 1;
            $link->update(['weight' => $weight]);
        }

        return;
    }
}
