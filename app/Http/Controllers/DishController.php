<?php

namespace App\Http\Controllers;

use App\Cuisine;
use App\Category;
use App\Dish;
use App\Helpers;
use Illuminate\Http\Request;
use App\Http\Requests\DishRequest;

class DishController extends Controller
{

    /**
     * Вывод списка всех блюд в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dishes = Dish::all();

        return view('admin.dishes.index', compact('dishes'));
    }

    /**
     * Форма добавления нового блюда
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        // Формируем список кухонь для формы
        $cuisines = [];
        foreach (Cuisine::all() as $item) {
            $cuisines[$item->id] = $item->title;
        }

        // Формируем список категорий для формы
        $categories = [];
        foreach (Category::all() as $item) {
            $categories[$item->id] = $item->title;
        }

        $data = [
            'cuisines'   => $cuisines,
            'categories' => $categories,
        ];

        return view('admin.dishes.create', $data);
    }

    /**
     * Сохранить новое блюдо
     *
     * @param \App\Http\Requests\DishRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DishRequest $request)
    {
        // Если передан файл, сохраняем новый файл,
        // в ответ получаем имя файла
        $image_name = Helpers::storeImage($request, new Dish);

        // Добавляем поле с именем файла
        $request->request->add(['image' => $image_name]);

        // Сохраняем запись
        Dish::create($request->all());

        return redirect()->route('admin.dishes.index')
            ->with("success", "Запись успешно создана!");
    }

    /**
     * Форма редактирования блюда
     *
     * @param \App\Dish $dish
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Dish $dish)
    {
        // Формируем список кухонь для формы
        $cuisines = [];
        foreach (Cuisine::all() as $item) {
            $cuisines[$item->id] = $item->title;
        }

        // Формируем список категорий для формы
        $categories = [];
        foreach (Category::all() as $item) {
            $categories[$item->id] = $item->title;
        }

        $data = [
            'dish'       => $dish,
            'cuisines'   => $cuisines,
            'categories' => $categories,
        ];

        return view('admin.dishes.edit', $data);
    }

    /**
     * Обновить блюдо
     *
     * @param \App\Http\Requests\DishRequest $request
     * @param \App\Dish                      $dish
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DishRequest $request, Dish $dish)
    {
        // Если передан файл, сохраняем новый файл, удалив старый,
        // в ответ получаем имя файла
        $image_name = Helpers::replaceImage($request, $dish);

        // Добавляем поле с именем файла
        $request->request->add(['image' => $image_name]);

        // Обновляем запись
        $dish->update($request->all());

        return redirect()->back()
            ->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление блюда
     *
     * @param Request $request
     * @param Dish    $dish
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Dish $dish)
    {
        // Если перешли методом delete, удаляем запись
        if ($request->isMethod('delete')) {
            $dish->delete();

            return redirect()->route('admin.dishes.index')
                ->with("success", "Запись удалена!");
        }

        return view('admin.dishes.delete', compact('dish'));
    }
}
