<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Review;
use Illuminate\Http\Request;
use App\User;
use App\Page;
use App\TopNav;
use App\SideNav;
use App\Cuisine;
use App\Category;
use App\Dish;
use App\Cook;
use App\Order;
use Auth;

class AdminController extends Controller
{

    /**
     * Главная страница админки
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'users'         => User::count(),
            'pages'         => Page::count(),
            'cuisines'      => Cuisine::count(),
            'categories'    => Category::count(),
            'dishes'        => Dish::count(),
            'cooks'         => Cook::count(),
            'ordersActive'  => Order::active()->count(),
            'orders'        => Order::count(),
            'reviewsActive' => Review::active()->count(),
            'reviews'       => Review::count(),
            'menus'         => Menu::count(),
        ];

        return view('admin.index', $data);
    }
}
