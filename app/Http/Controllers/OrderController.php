<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;
use Illuminate\Http\Request;
use Auth;

class OrderController extends Controller
{

    /**
     * Вывод всех заказов в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.orders.index', ['orders' => Order::all()]);
    }


    /**
     * Форма редактирования заказа
     *
     * @param \App\Order $order
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Order $order)
    {
        $cart = json_decode($order->cart);

        return view('admin.orders.edit', compact('order', 'cart'));
    }

    /**
     * Обновление заказа
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Order               $order
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Order $order)
    {
        $order->details->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }
}
