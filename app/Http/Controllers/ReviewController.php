<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Вывод всех отзывов в админке
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.reviews.index', ['reviews' => Review::all()]);
    }

    /**
     * Форма добавления отзыва
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.reviews.create');
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Форма редактирования отзыва
     *
     * @param  \App\Review $review
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        return view('admin.reviews.edit', compact('review'));
    }

    /**
     * Обновление отзыва
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Review              $review
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        $review->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление отзыва
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Review              $review
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Review $review)
    {
        // Если перешли методом delete, удаляем запись
        if ($request->isMethod('delete')) {
            $review->delete();

            return redirect()->route('admin.reviews.index')
                ->with("success", "Запись удалена!");
        }

        return view('admin.reviews.delete', compact('review'));
    }
}
