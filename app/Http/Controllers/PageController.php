<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;
use App\Page;
use App\Cuisine;
use App\Category;
use App\Dish;
use App\Cook;
use App\User;
use Auth;

class PageController extends Controller
{

    /**
     * Вывод списка всех страниц в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.index', ['pages' => Page::all()]);
    }

    /**
     * Форма добавления страницы
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Сохранение страницы
     *
     * @param \App\Http\Requests\PageRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PageRequest $request)
    {
        Page::create($request->all());

        return redirect()->route('admin.pages.index')
            ->with("success", "Запись успешно создана!");
    }

    /**
     * Форма редактирования страницы
     *
     * @param \App\Page $page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Page $page)
    {
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Обновление страницы
     *
     * @param \App\Http\Requests\PageRequest $request
     * @param \App\Page                      $page
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PageRequest $request, Page $page)
    {
        $page->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление страницы
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Page                $page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Page $page)
    {
        // Если перешли методом delete, удаляем запись
        if ($request->isMethod('delete')) {
            $page->delete();

            return redirect()->route('admin.pages.index')
                ->with("success", "Запись удалена!");
        }

        return view('admin.pages.delete', compact('page'));
    }
}
