<?php

namespace App\Http\Controllers;

use App\Dish;
use App\Helpers;
use App\Http\Requests\CategoryRequest;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * Вывод списка категорий
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Отображение формы создания новой категории
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Сохранение новой категории
     *
     * @param \App\Http\Requests\CategoryRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        // Если передан файл, сохраняем новый файл,
        // в ответ получаем имя файла
        $image_name = Helpers::storeImage($request, new Category);

        // Добавляем поле с именем файла
        $request->request->add(['image' => $image_name]);

        // Сохраняем запись
        Category::create($request->all());

        return redirect()->route('admin.categories.index')
            ->with("success", "Запись успешно создана!");
    }


    /**
     * Отображение формы редактирования категории
     *
     * @param \App\Category $category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Обновление категории
     *
     * @param \App\Http\Requests\CategoryRequest $request
     * @param \App\Category                      $category
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryRequest $request, Category $category)
    {
        // Если передан файл, сохраняем новый файл, удалив старый,
        // в ответ получаем имя файла
        $image_name = Helpers::replaceImage($request, $category);

        // Добавляем поле с именем файла
        $request->request->add(['image' => $image_name]);

        // Обновляем запись
        $category->update($request->all());

        return redirect()->route('admin.categories.index')
            ->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление выбранного раздела меню
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Category            $category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Category $category)
    {
        // Если перешли методом delete, удаляем запись
        if ($request->isMethod('delete')) {
            $category->delete();

            return redirect()->route('admin.categories.index')
                ->with("success", "Запись удалена!");
        }

        return view('admin.categories.delete', compact('category'));
    }
}
