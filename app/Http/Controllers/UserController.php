<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\UserProfile;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

    /**
     * Вывод таблицы всех пользователей в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.users.index', ['users' => User::all()]);
    }

    /**
     * Форма создания нового пользователя в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Сохранение нового пользователя из админки
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        // Модифицируем и добавляем поля
        $request->merge([
            'password' => bcrypt($request->input('password')),
            'verified' => 1
        ]);

        // Сохраняем пользователя
        $user = User::create($request->all());

        // Создаем профиль пользователя
        $user->profile()->save(new UserProfile);

        return redirect()->route('admin.users.index')
            ->with("success", "Запись успешно создана!");
    }

    /**
     * Страница редактирования пользователя в админке.
     *
     * @param \App\User $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Изменить данные пользователя из админки.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User                $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User $user)
    {
        // Обновляем запись
        $user->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }

    /**
     * Удалить пользователя из админки.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User                $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, User $user)
    {
        // Если перешли методом delete, удаляем запись
        if ($request->isMethod('delete')) {
            $user->delete();

            return redirect()->route('admin.users.index')
                ->with("success", "Запись удалена!");
        }

        return view('admin.users.delete', compact('user'));
    }
}
