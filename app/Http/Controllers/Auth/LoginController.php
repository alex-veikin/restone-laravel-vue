<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;

class LoginController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'guest' )->except( 'logout' );
	}

	//Используем логин вместо email для входа
	public function username() {
		return 'login';
	}


	/**
	 * Переопределяем метод входа на сайт
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return $this|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response|void
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function login( Request $request) {
		$validator = Validator::make( $request->all(),
			[
				$this->username() => 'required|alpha_dash|exists:users,login',
				'password'        => 'required|string',
			],
			[
				'exists' => 'Неверный логин.',
			]
		);

		if ( $validator->fails() ) {
			return redirect()->route( 'login' )
			                 ->withErrors( $validator )
			                 ->withInput();
		}

		if ( $this->hasTooManyLoginAttempts( $request ) ) {
			$this->fireLockoutEvent( $request );

			return $this->sendLockoutResponse( $request );
		}

		if ( $this->attemptLogin( $request ) ) {
			return $this->sendLoginResponse( $request );
		}

		$this->incrementLoginAttempts( $request );

		return $this->sendFailedLoginResponse( $request );
	}
}
