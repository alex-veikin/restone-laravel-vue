<?php

namespace App\Http\Controllers;

use App\Cook;
use App\Helpers;
use Illuminate\Http\Request;
use App\Http\Requests\CookRequest;

class CookController extends Controller
{
    /**
     * Вывод списка поваров
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cooks = Cook::all()->sortBy('weight');

        return view('admin.cooks.index', compact('cooks'));
    }

    /**
     * Отображение формы создания нового повара
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.cooks.create');
    }

    /**
     * Сохранение нового повара
     *
     * @param \App\Http\Requests\CookRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CookRequest $request)
    {
        // Если передан файл, сохраняем новый файл,
        // в ответ получаем имя файла
        $image_name = Helpers::storeImage($request, new Cook);

        // Добавляем поле с именем файла
        $request->request->add(['image' => $image_name]);

        // Сохраняем запись
        Cook::create($request->all());

        return redirect()->route('admin.cooks.index')
            ->with("success", "Запись успешно создана!");
    }

    /**
     * Отображение формы редактирования выбранного раздела меню
     *
     * @param \App\Cook $cook
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Cook $cook)
    {
        return view('admin.cooks.edit', compact('cook'));
    }

    /**
     * Обновление выбранного повара
     *
     * @param \App\Http\Requests\CookRequest $request
     * @param \App\Cook                      $cook
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CookRequest $request, Cook $cook)
    {
        // Если передан файл, сохраняем новый файл, удалив старый,
        // в ответ получаем имя файла
        $image_name = Helpers::replaceImage( $request, $cook );

        // Добавляем поле с именем файла
        $request->request->add(['image' => $image_name]);

        // Обновляем запись
        $cook->update($request->all());

        return redirect()->route('admin.cooks.index')
            ->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление выбранного раздела меню
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Cook                $cook
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Cook $cook)
    {
        if ($request->isMethod('delete')) {
            $cook->delete();

            return redirect()->route('admin.cooks.index')
                ->with("success", "Запись удалена!");
        }

        return view('admin.cooks.delete', compact('cook'));
    }
}
