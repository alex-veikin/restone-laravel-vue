<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cart;
use App\Dish;

class CartController extends Controller
{

	/**
	 * Get cart Api
	 *
	 * @return \Illuminate\Http\JsonResponse|null
	 */
	public function index()
	{
		//Получаем корзину
		$cart = new Cart();
		$data = [
			'items'       => $cart->items,
			'totalQty'   => $cart->totalQty,
			'totalPrice' => $cart->totalPrice,
		];

		//Возвращаем корзину, если она есть, или null
		return session()->has('cart') ? response()->json(['cart' => $data]) : null;
	}

	/**
	 * Increase item count Api
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse|null
	 */
	public function increaseItem($id)
	{
		//Получаем корзину, увеличиваем кол-во товара, записываем в сессию
		$cart = new Cart();
		$cart->increase(Dish::find($id));
		session(['cart' => $cart]);

		//Возвращаем корзину
		return $this->index();
	}

	/**
	 * Decrease item count Api
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse|null
	 */
	public function decreaseItem($id)
	{
		//Получаем корзину, уменьшаем кол-во товара, записываем в сессию
		$dish = Dish::find($id);
		$cart = new Cart();
		$cart->decrease($dish);
		if (session()->has('cart')) {
			session(['cart' => $cart]);
		}

		//Возвращаем корзину
		return $this->index();
	}

	/**
	 * Remove item Api
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse|null
	 */
	public function removeItem($id)
	{
		//Получаем корзину, удаляем товар, записываем в сессию
		$dish = Dish::find($id);
		$cart = new Cart();
		$cart->remove($dish);
		if (session()->has('cart')) {
			session(['cart' => $cart]);
		}

		//Возвращаем корзину
		return $this->index();
	}

	/**
	 * Clear cart Api
	 *
	 * @return \Illuminate\Http\JsonResponse|null
	 */
	public function clearCart()
	{
		//Удаляем корзину
		Cart::clear();

		//Возвращаем корзину
		return $this->index();
	}
}
