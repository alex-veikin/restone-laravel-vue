<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Dish;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

	/**
	 * Get categories Api
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index()
	{
		return response()->json(['categories' => Category::all()]);
	}


	/**
	 * Show category Api
	 *
	 * @param $alias
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show($alias)
	{
		// Получаем категорию и ее блюда
		$category = Category::where('alias', $alias)->first();
		$dishes = Dish::where('category_id', $category->id)
		              ->with(['cuisine', 'category'])
		              ->paginate(6);

		$data = [
			'category' => $category,
			'dishes' => $dishes,
		];

		return response()->json($data);
	}
}
