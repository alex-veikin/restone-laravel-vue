<?php

namespace App\Http\Controllers\Api;

use App\Events\UserRegistered;
use App\UserProfile;
use App\VerificationToken;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\User;
use Event;
use Mail;
use App\Mail\UserVerificationMail;

class AuthController extends Controller {

	use AuthenticatesUsers;

	/**
	 * Login Api
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function login( Request $request ) {
		// Валидируем данные
		$validator = Validator::make( $request->all(),
			[
				'login'    => 'required|alpha_dash',
				'password' => 'required|string',
			]
		);

		// Если не прошли валидацию, возвращаем ошибки
		if ( $validator->fails() ) {
			return response()->json( [ 'errors' => $validator->errors() ] );
		}

		$user = User::where( 'login', $request->login )->first();

		// Проверяем введенные данные
		if ( $user && \Hash::check( $request->password, $user->password ) ) {
			// Если пользователь не верифицирован,
			// то возвращаем статус верификации
			if ( ! $user->verified ) {
				return response()->json( [ 'verified' => 0 ] );
			}

			// Иначе аутентифицируем пользователя,
			// возвращаем ответ
			if ( Auth::attempt( [
				'login'    => $request->login,
				'password' => $request->password,
			], $request->remember )
			) {
				return response()->json( [ 'verified' => 1 ] );
			}
		}

		// Если пользователя с такими данными не существует,
		// или данные введены неверно - возвращаем ошибку
		return response()->json( [ 'errorAuth' => true ] );
	}


	/**
	 * Logout Api
	 *
	 * @return void
	 */
	public function logout() {
		//Делаем логаут, отдаем ответ
		Auth::logout();
	}


	/**
	 * Register Api
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function register( Request $request ) {
		// Валидируем данные
		$validator = Validator::make( $request->all(), [
			'first_name' => 'required|regex:/[А-Яа-яЁё]/u|max:60',
			'last_name'  => 'required|regex:/[А-Яа-яЁё]/u|max:60',
			'login'      => 'required|alpha_dash|max:255|unique:users',
			'email'      => 'required|email|max:255|unique:users',
			'password'   => 'required|string|min:6|confirmed',
		] );

		// Если не прошли валидацию, возвращаем ошибки
		if ( $validator->fails() ) {
			return response()->json( [ 'errors' => $validator->errors() ] );
		}

		// Если прошли валидацию, регистрируем пользователя
		$user = User::create( [
			'first_name' => $request->first_name,
			'last_name'  => $request->last_name,
			'login'      => $request->login,
			'email'      => $request->email,
			'password'   => bcrypt( $request->password ),
		] );

		// Создаем профиль пользователя
		$user->profile()->save( new UserProfile );

		// Создаем токен верификации
		$user->verificationToken()->create( [ 'token' => str_random( 32 ) ] );

		// Вызываем событие
		event( new UserRegistered( $user ) );

		// Отдаем ответ
		return response()->json( [ 'registered' => $user ? true : false ] );
	}

	/**
	 * Get user Api
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getUser() {
		// Если пользователь аутентифицирован, получаем его, иначе null
		$user = Auth::user() ? Auth::user()->load( [
			'orders.details',
			'profile',
			'review',
			'newEmail',
		] ) : null;

		// Возврашаем пользователя
		return response()->json( [ 'user' => $user ], 200 );
	}

	/**
	 * Verify user or new email
	 */
	public function activate( $token ) {
		// Находим токен
		$verification = VerificationToken::where( 'token', $token )->first();

		// Если токен есть
		if ($verification) {
			// Находим пользователя по токену
			$user = $verification->user()->first()->load('newEmail');

			// Если пользователь указал новый email
			if ($user->newEmail) {
				// Обновляем email
				$user->update( [ 'email' => $user->newEmail->email ] );
				$user->newEmail->delete();
			} else {
				// Иначе просто верифицируем пользователя
				$user->update( [ 'verified' => 1 ] );
			}

			// Удаляем токен
			$verification->delete();
		}

		// Переходим на главную
		return redirect( '/' );
	}
}
