<?php

namespace App\Http\Controllers\Api;

use App\OrderDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use Auth;
use App\Cart;
use Stripe\Charge;
use Stripe\Stripe;

class OrderController extends Controller {

	/**
	 * Get user orders Api
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index() {
		//Получаем все заказы пользователя
		$orders = Auth::user()->orders()->get()->load( 'details' );

		//Декодируем данные
		foreach ( $orders as $order ) {
			$order->cart = json_decode( $order->cart );
		}

		//Возвращаем заказы
		return response()->json( [ 'userOrders' => $orders ] );
	}

	/**
	 * Get user order by id Api
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show( $id ) {
		//Ишем заказ и декодируем
		$order       = Order::find( $id )->load( 'details' );
		$order->cart = json_decode( $order->cart );

		//Возвращаем
		return response()->json( [ 'order' => $order ] );
	}


	/**
	 * Сохранение заказа в базу
	 *
	 * @return $this|\Illuminate\Database\Eloquent\Model|null
	 */
	public function store() {
		//Получаем пользователя
		$user = Auth::user();

		//Получаем корзину
		$cart = new Cart();
		$data = [
			'items'      => $cart->items,
			'totalQty'   => $cart->totalQty,
			'totalPrice' => $cart->totalPrice,
		];

		//Если корзина есть
		if ( $cart ) {
			//Сохраняем заказ
			$order = Order::create( [
				//если пользователь есть, указываем его id
				'user_id' => $user ? $user->id : null,
				'cart'    => json_encode( $data ),
			] );

			//Если заказ сохранен
			if ( $order ) {
				//Очищаем корзину
				Cart::clear();

				//Возвращаем заказ
				return $order;
			}
		}

		//Если корзина пустая возвращаем null
		return null;
	}

	/**
	 * Обработка оформления заказа
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function checkout( Request $request ) {
		//Сохраняем заказ
		$order   = $this->store();
		$orderId = $order->id;

		//Сохраняем детализацию
		$orderDetails = OrderDetails::create(
			[
				'order_id' => $orderId,
				'name'     => $request->name,
				'email'    => $request->email,
				'phone'    => $request->phone,
				'shipping' => $request->shipping,
				'address'  => $request->address,
			]
		);

		//Если выбрана онлайн оплата
		if ( $request->payment ) {
			// Оплачиваем
			$charge = $this->payment( $request, $orderId );

			//Если оплата прошла успешно
			if ( $charge->paid ) {
				//Меняем статус заказа
				$orderDetails->status = 0;
				$orderDetails->save();

				//Возврашаем ответ [успешно, оплачено]
				return response()->json( [
					'success' => true,
					'paid'    => true,
					'order'   => Order::find( $orderId )->load( 'details' )
				] );
			}

			//Иначе возвращаем ответ [неуспешно, неоплачено]
			return response()->json( [ 'success' => false, 'paid' => false ] );
		}

		//Если выбрана оплата наличными
		//Возврашаем ответ [успешно, неоплачено]
		return response()->json( [
			'success' => true,
			'paid'    => false,
			'order'   => Order::find( $orderId )->load( 'details' )
		] );
	}


	/**
	 * Оплата заказа через Stripe
	 *
	 * @param $request
	 * @param $orderId
	 *
	 * @return \Stripe\ApiResource
	 */
	public function payment( $request, $orderId ) {
		Stripe::setApiKey( config( 'services.stripe.secret' ) );

		// Оплачиваем, указав детали
		return Charge::create( [
			'amount'        => $request->amount * 100 / 2,
			'currency'      => 'usd',
			'source'        => $request->stripeToken,
			'description'   => 'Заказ: #' . $orderId .
			                   ' , сумма: ' . $request->amount .
			                   ' , email: ' . $request->email .
			                   ' , телефон: ' . $request->phone .
			                   ' , адрес доставки: ' . $request->address,
			'receipt_email' => $request->email,
		] );
	}
}
