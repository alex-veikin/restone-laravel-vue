<?php

namespace App\Http\Controllers\Api;

use App\Menu;
use App\Page;
use App\Http\Controllers\Controller;
use App\Cuisine;
use App\Category;
use App\Cook;
use File;

class AppController extends Controller {

	/**
	 * Get items Api
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index() {
		$data = [
			'cuisines'   => Cuisine::all(),
			'categories' => Category::all(),
			'cooks'      => Cook::all()->sortBy( 'weight' ),
			'menus'      => Menu::all()->load( 'items.page' ),
		];

		return response()->json( $data );
	}

	/**
	 * Get page Api
	 *
	 * @param $alias
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function page( $alias ) {
		$images = [];

		// Получаем все изображения страницы
		if ( File::exists( public_path( '/img/' . $alias ) ) ) {
			$files = File::allFiles( public_path( '/img/' . $alias ) );

			foreach ( $files as $image ) {
				$images[] = $image->getFilename();
			}
		}

		// Получаем страницу
		$page = Page::where( 'alias', $alias )->first();

		// Отдаем ответ
		return response()->json( [ 'page' => $page, 'images' => $images ] );
	}
}
