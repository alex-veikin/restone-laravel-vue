<?php

namespace App\Http\Controllers\Api;

use App\Events\UserEmailChanged;
use App\NewUserEmail;
use App\Rules\CheckPassword;
use App\Rules\ComparePassword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use Auth;
use Image;
use File;

class UserController extends Controller {

	/**
	 * Изменить данные пользователя.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function updateProfile( Request $request ) {
		// Валидируем данные
		$validator = Validator::make( $request->all(), [
			'first_name' => 'required|regex:/[А-Яа-яЁё]/u|max:60',
			'last_name'  => 'required|regex:/[А-Яа-яЁё]/u|max:60',
			'password'   => [
				'required',
				'string',
				'min:6',
				new CheckPassword(),
			],
		] );

		// Если не прошли валидацию, возвращаем ошибки
		if ( $validator->fails() ) {
			return response()->json( [ 'errors' => $validator->errors() ] );
		}

		$user = Auth::user();

		// Новые данные
		$user->first_name = $request->first_name;
		$user->last_name  = $request->last_name;

		// Если нет ошибок, сохраняем новые данные
		return response()->json( [ 'success' => $user->save() ] );
	}


	/**
	 * Изменить пароль пользователя.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function updatePassword( Request $request ) {
		// Валидируем данные
		$validator = Validator::make( $request->all(), [
			'password'     => [
				'required',
				new CheckPassword(),
			],
			'password_new' => [
				'required',
				'string',
				'min:6',
				new ComparePassword(),
				'confirmed',
			],
		] );

		// Если не прошли валидацию, возвращаем ошибки
		if ( $validator->fails() ) {
			return response()->json( [ 'errors' => $validator->errors() ] );
		}

		$user = Auth::user();

		// Новые данные
		$user->password = bcrypt( $request->password_new );

		// Если нет ошибок, сохраняем новые данные
		return response()->json( [ 'success' => $user->save() ] );
	}


	/**
	 * Изменить аватарку пользователя
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updateAvatar( Request $request ) {
		// Валидируем данные
		$validator = Validator::make( $request->all(), [
			'image' => 'required|image64:jpeg,jpg,png',
		] );

		// Если не прошли валидацию, возвращаем ошибки
		if ( $validator->fails() ) {
			return response()->json( [ 'errors' => $validator->errors() ] );
		}

		$user = Auth::user();

		// Получаем файл
		$image = $request->image;

		// Формируем уникальное имя с родным расширением
		$image_name = round( microtime( true ) * 1000 ) . '.'
		              . explode( '/', explode( ':',
				substr( $image, 0, strpos( $image, ';' ) ) )[1] )[1];

		// Если уже есть аватар, то удаляем его
		if ( $user->profile->avatar ) {
			File::delete( public_path( 'img/users/avatars/' )
			              . $user->profile->avatar );
		}

		// Сохраняем новый аватар под указанным именем в указанную папку
		Image::make( $request->image )
		     ->save( public_path( 'img/users/avatars/' ) . $image_name );

		// Новые данные
		$user->profile->avatar = $image_name;

		// Если успешно сохранены данные, возвращаем ответ
		if ( $user->profile->save() ) {
			return response()->json( [
				'success' => true,
				'avatar'  => $image_name,
			] );
		} else { // Иначе возвращаем ответ с неудачей
			return response()->json( [ 'success' => false ] );
		}
	}


	public function updateContacts( Request $request ) {
		$user = Auth::user();

		// Валидируем данные
		$validator = Validator::make( $request->all(), [
			'email'    => 'required|email|max:255|unique:users,email,'
			              . $user->id,
			'phone'    => 'string|nullable|size:19'
			              . '|regex:/^\+375 \([0-9]{2,3}\) [0-9]{2,3}-[0-9]{2}-[0-9]{2}$/'
			              . '|unique:user_profiles,phone,' . $user->profile->id,
			'address'  => 'string|nullable|max:255',
			'password' => [
				'required',
				'string',
				'min:6',
				new CheckPassword(),
			],
		] );

		// Если не прошли валидацию, возвращаем ошибки
		if ( $validator->fails() ) {
			return response()->json( [ 'errors' => $validator->errors() ] );
		}

		// Новые данные
		$user->profile->phone   = $request->phone;
		$user->profile->address = $request->address;

		// Если новый email отличается от старого
		if ( $user->email !== $request->email ) {
			// Создаем токен верификации
			$user->verificationToken()
			     ->updateOrCreate( [], [ 'token' => str_random( 32 ) ] );

			// Записываем новый email в базу
			$new_email = $user
				->newEmail()
				->updateOrCreate( [], [ 'email' => $request->email ] );

			// Вызываем событие отправки почты
			event( new UserEmailChanged( $user ) );

			// Сохраняем новые данные пользователя, возвращаем ответ
			return response()->json(
				[
					'success'   => $user->profile->save(),
					'new_email' => $new_email,
				]
			);
		}

		// Ели email не изменен, то просто сохраняем новые данные пользователя
		return response()->json( [ 'success' => ( $user->profile->save() ) ] );
	}
}
