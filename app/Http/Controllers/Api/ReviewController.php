<?php

namespace App\Http\Controllers\Api;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class ReviewController extends Controller {

	/**
	 * Get reviews Api
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index() {
		return response()->json( Review::where('status', 0)
		                               ->with(['user.profile'])
		                               ->orderByDesc('created_at')
		                               ->paginate(6) );
	}


	/**
	 * Store a review Api
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store( Request $request ) {
		// Валидируем данные
		$validator = Validator::make( $request->all(), [
			'rating' => 'required|numeric',
			'text' => 'required|string|max:255',
		] );

		// Если не прошли валидацию, возвращаем ошибки
		if ( $validator->fails() ) {
			return response()->json( [ 'errors' => $validator->errors() ] );
		}

		$user = Auth::user();

		// Сохраняем отзыв, возвращаем ответ
		return response()->json( [
			'success' => $user->review()->create( $request->all() ),
		] );
	}
}
