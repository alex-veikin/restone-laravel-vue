<?php

namespace App\Http\Controllers\Api;

use App\Dish;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DishController extends Controller
{

	/**
	 * Get dishes Api
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index()
	{
		return response()->json(Dish::with(['cuisine', 'category'])->paginate(6));
	}

	/**
	 * Show dish Api
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show($id)
	{
		// Получаем блюдо и рекомендованные блюда
		$dish = Dish::find($id);
		$recommended = Dish::where('category_id', $dish->category->id)
		                   ->orWhere('cuisine_id', $dish->cuisine->id)
		                   ->with(['category', 'cuisine'])
		                   ->get()
		                   ->where('id', '!=', $dish->id)
		                   ->shuffle();

		return response()->json(['dish' => $dish, 'recommended' => $recommended]);
	}
}
