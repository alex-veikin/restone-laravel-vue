<?php

namespace App\Http\Controllers\Api;

use App\Cuisine;
use App\Dish;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CuisineController extends Controller
{

	/**
	 * Get cuisines Api
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index()
	{
		return response()->json(['cuisines' => Cuisine::all()]);
	}

	/**
	 * Show cuisine Api
	 *
	 * @param $alias
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show($alias)
	{
		// Получаем кухню и ее блюда
		$cuisine = Cuisine::where('alias', $alias)->first();
		$dishes = Dish::where('cuisine_id', $cuisine->id)
		              ->with(['cuisine', 'category'])
		              ->paginate(6);

		$data = [
			'cuisine' => $cuisine,
			'dishes' => $dishes,
		];

		return response()->json($data);
	}
}
