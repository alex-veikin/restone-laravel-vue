<?php

namespace App\Http\Controllers;

use App\Cuisine;
use App\Helpers;
use Illuminate\Http\Request;
use App\Dish;
use App\Http\Requests\CuisineRequest;

class CuisineController extends Controller
{
    /**
     * Вывод списка пунктов кухни в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cuisines = Cuisine::all();

        return view('admin.cuisines.index', compact('cuisines'));
    }

    /**
     * Отображение формы создания новой кухни
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.cuisines.create');
    }

    /**
     * Сохранение новой кухни
     *
     * @param \App\Http\Requests\CuisineRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CuisineRequest $request)
    {
        // Если передан файл, сохраняем новый файл,
        // в ответ получаем имя файла
        $image_name = Helpers::storeImage($request, new Cuisine);

        // Добавляем поле с именем файла
        $request->request->add(['image' => $image_name]);

        // Сохраняем запись
        Cuisine::create($request->all());

        return redirect()->route('admin.cuisines.index')
            ->with("success", "Запись успешно создана!");
    }

    /**
     * Отображение формы редактирования выбранной кухни
     *
     * @param \App\Cuisine $cuisine
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Cuisine $cuisine)
    {
        return view('admin.cuisines.edit', compact('cuisine'));
    }

    /**
     * Обновление выбранной кухни
     *
     * @param \App\Http\Requests\CuisineRequest $request
     * @param \App\Cuisine                      $cuisine
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CuisineRequest $request, Cuisine $cuisine)
    {
        // Если передан файл, сохраняем новый файл, удалив старый,
        // в ответ получаем имя файла
        $image_name = Helpers::replaceImage( $request, $cuisine );

        // Добавляем поле с именем файла
        $request->request->add(['image' => $image_name]);

        // Обновляем запись
        $cuisine->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление выбранной кухни
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Cuisine             $cuisine
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Cuisine $cuisine)
    {
        // Если перешли методом delete, удаляем запись
        if ($request->isMethod('delete')) {
            $cuisine->delete();

            return redirect()->route('admin.cuisines.index')
                ->with("success", "Запись удалена!");
        }

        return view('admin.cuisines.delete', compact('cuisine'));
    }
}
