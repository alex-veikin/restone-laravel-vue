<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['user_id', 'cart'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function details()
    {
        return $this->hasOne(OrderDetails::class);
    }

    public function scopeActive($query)
    {
        return $query->whereHas('details', function ($query) {
            $query->where('status', 1);
        });
    }
}

