<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable =
        [
            'alias',
            'title',
            'description',
            'image',
        ];

    public static $image_size = ['width' => 540, 'height' => 560];

    public function dishes()
    {
        return $this->hasMany(Dish::class);
    }

    public static function boot() {
        static::deleting(function ($model) {
            \File::delete(public_path('img/categories/') . $model->image);
        });
    }
}
