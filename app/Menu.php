<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

	protected $fillable =
		[
			'alias',
			'title',
		];


	public function items()
	{
		return $this->hasMany(MenuItem::class);
	}
}
