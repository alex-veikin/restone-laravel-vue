<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
	protected $fillable = ['order_id', 'name', 'email', 'phone', 'shipping', 'address', 'status'];
	protected $table = 'order_details';

	public function order() {
		return $this->belongsTo(Order::class);
	}
}
