<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\TopNav;
use App\SideNav;

class NavServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->topNav();
        $this->sideNav();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    //Top Nav
    public function topNav()
    {
        View::composer('layouts.site', function ($view) {
            $view->with('top_nav',
                TopNav::with('page')->get()->sortBy('weight'));
        });
    }

    //Side Nav
    public function sideNav()
    {
        View::composer('layouts.site', function ($view) {
            $view->with('side_nav',
                SideNav::with('page')->get()->sortBy('weight'));
        });
    }
}
