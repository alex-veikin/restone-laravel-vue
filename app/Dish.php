<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{

    protected $fillable =
        [
            'cuisine_id',
            'category_id',
            'title',
            'description',
            'image',
            'price',
        ];

    public static $image_size = ['width' => 640, 'height' => 480];

    public function cuisine()
    {
        return $this->belongsTo(Cuisine::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public static function boot() {
        static::deleting(function ($model) {
            \File::delete(public_path('img/dishes/') . $model->image);
        });
    }
}
