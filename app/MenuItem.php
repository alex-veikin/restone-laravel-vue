<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{

	protected $fillable =
		[
			'menu_id',
			'type',
			'page_id',
			'url',
			'url_title',
			'submenu_title',
			'parent_id',
			'weight',
		];


	public function menu()
	{
		return $this->belongsTo(Menu::class);
	}


	public function page()
	{
		return $this->belongsTo(Page::class);
	}
}
