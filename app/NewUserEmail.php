<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewUserEmail extends Model
{

	protected $fillable = ['user_id', 'email'];

	public function user() {
		return $this->belongsTo(User::class);
	}
}
