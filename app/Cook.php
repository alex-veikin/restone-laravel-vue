<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cook extends Model
{

    protected $fillable =
        [
            'weight',
            'alias',
            'full_name',
            'title',
            'description',
            'image',
        ];

    public static $image_size = ['width' => 450, 'height' => 450];

    public static function boot() {
        static::deleting(function ($model) {
            \File::delete(public_path('img/cooks/') . $model->image);
        });
    }
}
