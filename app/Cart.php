<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

    // Определяем стандартные свойства
    public $items = null; // продукты
    public $totalQty = 0; // общее колличество товаров
    public $totalPrice = 0; // общая цена


	/**
	 * Создаем объект с данными корзины
	 */
    public function __construct()
    {
        // Извлекаем корзину из сессии
        $oldCart = session('cart', null);

        if ($oldCart) {
            // заменяем дефолтные значения
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }


	/**
	 * Добавляем продукт или увеличиваем количество
	 *
	 * @param $product
	 */
    public function increase($product)
    {
        // Если корзина не пустая и в ней уже есть передаваемый продукт
        if ($this->items && array_key_exists($product->id, $this->items)) {
            // извлекаем продукт
            $cartItem = $this->items[$product->id];

            // увеличиваем его количество
            $cartItem['qty']++;

            // и вычисляем сумму
            $cartItem['price'] = $product->price * $cartItem['qty'];
        } else {
            // иначе создаем массив с данными продукта
            $cartItem = [
                'qty'   => 1, // количество
                'price' => $product->price, // сумма
                'item'  => $product // продукт
            ];
        }

        // Сохраняем продукт
        $this->items[$product->id] = $cartItem;

        // Увеличеваем общее количество товаров
        $this->totalQty++;

        // Увеличеваем общую сумму
        $this->totalPrice += $product->price;
    }


	/**
	 * Уменьшаем количество или удаляем
	 *
	 * @param $product
	 */
    public function decrease($product)
    {
	    // Если корзина не пустая и в ней уже есть передаваемый продукт
        if ($this->items && array_key_exists($product->id, $this->items)) {
            // извлекаем продукт
            $cartItem = $this->items[$product->id];

            // уменьшаем его количество
            $cartItem['qty']--;

            // если количество равно нулю, удаляем продукт
            if (!$cartItem['qty']) {
                unset($this->items[$product->id]);
            } else {
                // иначе вычисляем сумму
                $cartItem['price'] = $product->price * $cartItem['qty'];

                // и сохраняем
                $this->items[$product->id] = $cartItem;
            }

            // Уменьшаем общее количество товаров
            $this->totalQty--;

            // если общее количество равно нулю, удаляем корзину
            if (!$this->totalQty) {
                self::clear();
            } else {
                // Уменьшаем общую сумму
                $this->totalPrice -= $product->price;
            }
        }
    }


	/**
	 * Удаляем пункт
	 *
	 * @param $product
	 */
    public function remove($product)
    {
	    // Если корзина не пустая и в ней уже есть передаваемый продукт
        if ($this->items && array_key_exists($product->id, $this->items)) {
            // извлекаем продукт
            $cartItem = $this->items[$product->id];

            // удаляем продукт
            unset($this->items[$product->id]);

            // Уменьшаем общее количество товаров
            $this->totalQty -= $cartItem['qty'];

            // если общее количество равно нулю, удаляем корзину
            if (!$this->totalQty) {
                self::clear();
            } else {
                // иначе уменьшаем общую сумму
                $this->totalPrice -= $cartItem['price'];
            }
        }
    }


	/**
	 * Удаляем корзину из сессии
	 */
    public static function clear()
    {
        session()->forget('cart');
    }

}
