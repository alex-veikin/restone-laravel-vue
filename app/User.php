<?php

namespace App;

use App\Events\UserRegistered;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Event;

class User extends Authenticatable {

	use HasApiTokens, Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable
		= [
			'role',
			'first_name',
			'last_name',
			'login',
			'email',
			'password',
			'verified',
		];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden
		= [
			'password',
			'remember_token',
		];

	public function profile() {
		return $this->hasOne( UserProfile::class );
	}

	public function orders() {
		return $this->hasMany( Order::class );
	}

	public function review() {
		return $this->hasOne( Review::class );
	}

	public function newEmail() {
		return $this->hasOne( NewUserEmail::class );
	}

	public function verificationToken() {
		return $this->hasOne( VerificationToken::class );
	}
}
