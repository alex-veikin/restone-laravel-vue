<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Image;

class Helpers
{

    /**
     * Создать уникальное имя файла на основе microtime,
     * с указанным префиксом, с оригинальным расширением
     *
     * @param             $file
     * @param string|null $prefix
     *
     * @return string
     */
    public static function createUniqueFileName($file, string $prefix = null)
    {
        return ($prefix ? $prefix . '_' : '')
            . round(microtime(true) * 1000) . '.'
            . $file->getClientOriginalExtension();
    }


    /**
     * Сохраняем изображения под указанным именем,
     * по указанному пути, с указанными размерами
     *
     * @param        $image
     * @param string $image_name
     * @param string $path_name
     * @param array  $image_size
     */
    public static function storeImageFile(
        $image,
        string $image_name,
        string $path_name,
        array $image_size
    ) {
        Image::make($image->getRealPath())
            ->fit($image_size['width'], $image_size['height'],
                function ($constraint) {
                    $constraint->upsize();
                })
            ->save(public_path() . '/img/' . $path_name . '/' . $image_name);
    }


    /**
     * Сохранить файл, если он передан, и вернуть его имя,
     * иначе вернуть имя файла по-умолчанию
     *
     * @param \Illuminate\Http\Request            $request
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return string
     */
    public static function storeImage(Request $request, Model $model)
    {
        // Если отправлен файл
        if ($request->hasFile('file')) {
            // Получаем файл
            $image = $request->file('file');

            // Префикс для нового имени файла
            $prefix = strtolower(class_basename($model));

            // Имя директории
            $path_name = $model->getTable();

            // Формируем уникальное имя файла
            $image_name = self::createUniqueFileName($image, $prefix);

            // Сохраняем в указанную директорию с указанным именем
            self::storeImageFile($image, $image_name, $path_name,
                $model::$image_size);
        } else {
            // Иначе указываем имя файла по-умолчанию
            $image_name = 'no-image.jpg';
        }

        // Возвращаем имя файла
        return $image_name;
    }


    /**
     * Сохранить файл, если он передан, и вернуть его имя,
     * иначе вернуть имя существующего файла
     *
     * @param \Illuminate\Http\Request            $request
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return mixed|string
     */
    public static function replaceImage(Request $request, Model $model)
    {
        // Если отправлен файл
        if ($request->hasFile('file')) {
            // Получаем файл
            $image = $request->file('file');

            // Префикс для нового имени файла
            $prefix = strtolower(class_basename($model));

            // Имя директории
            $path_name = $model->getTable();

            // Формируем уникальное имя файла
            $image_name = self::createUniqueFileName($image, $prefix);

            // Удаляем старый файл
            \File::delete(public_path() . '/img/' . $path_name . '/'
                . $model->image);

            // Сохраняем в указанную директорию с указанным именем
            self::storeImageFile($image, $image_name, $path_name,
                $model::$image_size);
        } else {
            // Иначе оставляем старое имя
            $image_name = $model->image;
        }

        // Возвращаем имя файла
        return $image_name;
    }
}