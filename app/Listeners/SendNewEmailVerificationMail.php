<?php

namespace App\Listeners;

use App\Events\UserEmailChanged;
use App\Mail\NewEmailVerificationMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendNewEmailVerificationMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	/**
	 * @param \App\Events\UserEmailChanged $event
	 */
    public function handle(UserEmailChanged $event)
    {
        Mail::to($event->user->newEmail->email)
            ->send(new NewEmailVerificationMail($event->user));
    }
}
