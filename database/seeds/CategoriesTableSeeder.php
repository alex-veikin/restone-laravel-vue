<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'alias'       => 'breakfast',
                'title'       => 'Завтраки',
                'description' => '',
                'image'       => 'breakfast.jpg',
            ],
            [
                'alias'       => 'salads',
                'title'       => 'Салаты',
                'description' => '',
                'image'       => 'salads.jpg',
            ],
            [
                'alias'       => 'snacks',
                'title'       => 'Закуски',
                'description' => '',
                'image'       => 'snacks.jpg',
            ],
            [
                'alias'       => 'soups',
                'title'       => 'Супы',
                'description' => '',
                'image'       => 'soups.jpg',
            ],
            [
                'alias'       => 'garnishes',
                'title'       => 'Гарниры',
                'description' => '',
                'image'       => 'garnishes.jpg',
            ],
            [
                'alias'       => 'pizza',
                'title'       => 'Пицца',
                'description' => '',
                'image'       => 'pizza.jpg',
            ],
            [
                'alias'       => 'dessert',
                'title'       => 'Десерты',
                'description' => '',
                'image'       => 'dessert.jpg',
            ],
            [
                'alias'       => 'drink',
                'title'       => 'Напитки',
                'description' => '',
                'image'       => 'drink.jpg',
            ],
            [
                'alias'       => 'seafood',
                'title'       => 'Морепродукты',
                'description' => '',
                'image'       => 'seafood.jpg',
            ],
            [
                'alias'       => 'pasta',
                'title'       => 'Паста',
                'description' => '',
                'image'       => 'pasta.jpg',
            ],
            [
                'alias'       => 'grill',
                'title'       => 'Гриль',
                'description' => '',
                'image'       => 'grill.jpg',
            ],
            [
                'alias'       => 'sushi',
                'title'       => 'Суши и роллы',
                'description' => '',
                'image'       => 'sushi.jpg',
            ],
        ]);
    }
}
