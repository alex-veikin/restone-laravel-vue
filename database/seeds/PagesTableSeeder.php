<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table( 'pages' )->insert( [
			[
				'alias'       => '/',
				'title'       => 'Главная',
				'description' => '',
				'content'     => '',
			],
			[
				'alias'       => 'cuisines',
				'title'       => 'Кухни',
				'description' => '',
				'content'     => '',
			],
			[
				'alias'       => 'categories',
				'title'       => 'Категории',
				'description' => '',
				'content'     => '',
			],
			[
				'alias'       => 'dishes',
				'title'       => 'Все блюда',
				'description' => '',
				'content'     => '',
			],
			[
				'alias'       => 'cooks',
				'title'       => 'Повара',
				'description' => '',
				'content'     => '',
			],
			[
				'alias'       => 'contacts',
				'title'       => 'Контакты',
				'description' => '',
				'content'     => '<p>Адрес: 246000, Беларусь, г. Гомель, проспект Победы, 18</p>
<p>&nbsp;</p>
<p>Email:&nbsp;info@rest-one.by</p>
<p>&nbsp;</p>
<p>Телефоны: +375 (44) 111 11 11, +375 (29) 111 11 11, +375 (25) 111 11 11</p>
<p>&nbsp;</p>
<p>Мы на карте:</p>
<p><iframe frameborder="0" height="200" scrolling="no" src="https://yandex.ru/map-widget/v1/?um=constructor%3A25c81b57ea6d49bd3589cca2bfbd96585f726383ff89cdb119b4f74d6761f223&amp;source=constructor" width="100%"></iframe></p>',
			],
			[
				'alias'       => 'shipping',
				'title'       => 'Доставка',
				'description' => '',
				'content'     => '<p>Наша служба доставки&nbsp;позволяет перенести атмосферу ресторана Rest One к вам домой. Ресторанная подача, пунктуальность и вежливость курьеров, все это создает эффект присутствия в нашем ресторане в любом уголке вашего города.</p>
<p>&nbsp;</p>
<p>Доставка производится каждый день с 9:00 до 22:00.</p>
<p>&nbsp;</p>
<p>Оплата заказ производится в белорусских рублях путем передачи наличных денежных средств курьеру, либо онлайн банковской пластиковой картой на странице заказа.</p>',
			],
			[
				'alias'       => 'about',
				'title'       => 'О нас',
				'description' => '',
				'content'     => '<p>Кухня - сердце любого дома. Именно здесь за чашечкой чая или кофе начинается новый день, сюда спешит семья, чтобы собраться на ужин, приходят друзья и, несомненно, здесь воплощаются в жизнь самые смелые кулинарные фантазии. Эту неповторимую атмосферу тепла и уюта, ароматов домашних блюд и неспешных разговоров мы и сохранили в нашем ресторане.</p>
<p>&nbsp;</p>
<p>Меню ресторана составлено очень разнообразно. Поэтому вас ждут самые любимые, и потому лучшие рецепты со всего мира. Кроме того, в нашей винной карте Вы сможете подобрать идеальное сочетание к любому блюду.</p>
<p>&nbsp;</p>
<p>Вы можете оценить изысканность наших блюд, не только находясь в нашем ресторане, но и заказав банкет на природе или в любом другом месте.</p>',
			],
			[
				'alias'       => 'reviews',
				'title'       => 'Отзывы',
				'description' => '',
				'content'     => '',
			],
			[
				'alias'       => 'cart',
				'title'       => 'Корзина',
				'description' => '',
				'content'     => '',
			],
			[
				'alias'       => 'how_to_order',
				'title'       => 'Как заказать',
				'description' => '',
				'content'     => '',
			],
			[
				'alias'       => 'how_to_pay',
				'title'       => 'Как оплатить',
				'description' => '',
				'content'     => '',
			],
		] );
	}
}
