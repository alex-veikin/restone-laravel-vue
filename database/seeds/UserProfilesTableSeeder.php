<?php

use Illuminate\Database\Seeder;

class UserProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('user_profiles')->insert([
		    [
			    'user_id' => 1,
			    'avatar' => '1530743901075.jpeg',
			    'phone' => '+375 (29) 222-22-22',
			    'address' => 'Гомель, пр. Победы 5, 22',
		    ],
		    [
			    'user_id' => 2,
			    'avatar' => '1530187727738.jpeg',
			    'phone' => '+375 (29) 111-11-11',
			    'address' => 'Гомель, ул. Советская, д. 34, кв. 15',
		    ],
		    [
			    'user_id' => 3,
			    'avatar' => '1531243077053.png',
			    'phone' => '+375 (29) 333-33-33',
			    'address' => 'Гомель, ул. Интернациональная, д. 12, кв. 77',
		    ],
	    ]);
    }
}
