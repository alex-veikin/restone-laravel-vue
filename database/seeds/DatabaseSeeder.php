<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
	        UsersTableSeeder::class,
	        UserProfilesTableSeeder::class,
	        PagesTableSeeder::class,
	        CuisinesTableSeeder::class,
	        CategoriesTableSeeder::class,
	        CooksTableSeeder::class,
	        DishesTableSeeder::class,
	        AdminNavLinksTableSeeder::class,
	        MenusTableSeeder::class,
	        MenuItemsTableSeeder::class,
        ]);
    }
}
