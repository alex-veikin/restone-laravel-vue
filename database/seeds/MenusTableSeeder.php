<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table( 'menus' )->insert( [
			[
				'alias' => 'top_nav',
				'title' => 'Верхнее меню',
			],
			[
				'alias' => 'side_nav',
				'title' => 'Боковое меню',
			],
			[
				'alias' => 'footer_nav1',
				'title' => 'Меню футера 1',
			],
			[
				'alias' => 'footer_nav2',
				'title' => 'Меню футера 2',
			],
		] );
	}
}
