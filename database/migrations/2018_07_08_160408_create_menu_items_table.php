<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
	        $table->string('type');
	        $table->integer('page_id')->nullable();
	        $table->string('url')->nullable();
	        $table->string('url_title')->nullable();
	        $table->string('submenu_title')->nullable();
	        $table->integer('parent_id')->default(0);
	        $table->integer('weight')->default(1);
            $table->timestamps();
	        $table->foreign('menu_id')
	              ->references('id')
	              ->on('menus')
	              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
