const mix = require('laravel-mix');
// const ImageminPlugin = require('imagemin-webpack-plugin').default;
// const CopyWebpackPlugin = require('copy-webpack-plugin');
// const imageminMozjpeg = require('imagemin-mozjpeg');

mix.webpackConfig({
    resolve:{
        alias: {
            'vue-router$': 'vue-router/dist/vue-router.common.js'
        }
    }
});

if (mix.inProduction()) {
    mix.webpackConfig({
        plugins: [
            // //Compress images
            // new CopyWebpackPlugin([{
            //     from: 'resources/assets/img',
            //     to: 'public/img'
            // }]),
            // new ImageminPlugin({
            //     test: /\.(jpe?g|png|gif|svg)$/i,
            //     pngquant: {quality: '65-80'},
            //     plugins: [
            //         imageminMozjpeg({
            //             quality: 65,
            //             //Set the maximum memory to use in kbytes
            //             maxmemory: 1000 * 512
            //         })
            //     ]
            // })
        ]
    });

    mix.version();
}

mix.options({
    // extractVueStyles: false,
    // processCssUrls: true,
    // uglify: {},
    // purifyCss: false,
    // purifyCss: {},
    postCss: [require('autoprefixer')],
    // clearConsole: false
});

mix
    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/admin.js', 'public/js')

    // .extract([
    //     'jquery',
    //     'owl.carousel',
    // ])

    // .autoload({
    //     jquery: ['$', 'window.jQuery', 'jQuery'],
    // })

    .styles([
        'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
        'node_modules/owl.carousel/dist/assets/owl.theme.default.min.css',
    ], 'public/css/vendor.css')

    .sass('resources/assets/sass/main.scss', 'public/css')
    .sass('resources/assets/sass/admin.scss', 'public/css')

    .copy('resources/assets/fonts/*', 'public/fonts')
    // .copy('resources/assets/img', 'public/img', false)

    .browserSync({
        proxy: 'restone.loc',
        open: false,
        host: 'localhost',
        "watchOptions": {
            usePolling: true
        },
    });