<?php

// Админпанель
Breadcrumbs::register('admin.index', function ($breadcrumbs) {
    $breadcrumbs->push('Админпанель', route('admin.index'));
});



// Админпанель > Пользователи
Breadcrumbs::register('admin.users.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Пользователи', route('admin.users.index'));
});

// Админпанель > Пользователи > Новая запись
Breadcrumbs::register('admin.users.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.users.index');
    $breadcrumbs->push('Новая запись', route('admin.users.create'));
});

// Админпанель > Пользователи > [User]
Breadcrumbs::register('admin.users.edit', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('admin.users.index');
    $breadcrumbs->push($user->first_name.' '.$user->last_name, route('admin.users.edit', $user));
});

// Админпанель > Пользователи > [User] > Удалить
Breadcrumbs::register('admin.users.delete', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('admin.users.edit', $user);
    $breadcrumbs->push('Удалить', route('admin.users.delete', $user));
});



// Админпанель > Страницы
Breadcrumbs::register('admin.pages.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Страницы', route('admin.pages.index'));
});

// Админпанель > Страницы > Новая запись
Breadcrumbs::register('admin.pages.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.pages.index');
    $breadcrumbs->push('Новая запись', route('admin.pages.create'));
});

// Админпанель > Страницы > [Page]
Breadcrumbs::register('admin.pages.edit', function ($breadcrumbs, $page) {
    $breadcrumbs->parent('admin.pages.index');
    $breadcrumbs->push($page->title, route('admin.pages.edit', $page));
});

// Админпанель > Страницы > [Page] > Удалить
Breadcrumbs::register('admin.pages.delete', function ($breadcrumbs, $page) {
    $breadcrumbs->parent('admin.pages.edit', $page);
    $breadcrumbs->push('Удалить', route('admin.pages.delete', $page));
});



// Админпанель > Меню
Breadcrumbs::register('admin.menus.index', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.index');
	$breadcrumbs->push('Меню', route('admin.menus.index'));
});

// Админпанель > Меню > [Menu]
Breadcrumbs::register('admin.menus.show', function ($breadcrumbs, $menu) {
	$breadcrumbs->parent('admin.menus.index');
	$breadcrumbs->push($menu->title, route('admin.menus.show', $menu));
});



// Админпанель > Кухни
Breadcrumbs::register('admin.cuisines.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Кухни', route('admin.cuisines.index'));
});

// Админпанель > Кухни > Новая запись
Breadcrumbs::register('admin.cuisines.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.cuisines.index');
    $breadcrumbs->push('Новая запись', route('admin.cuisines.create'));
});

// Админпанель > Кухни > [Cuisine]
Breadcrumbs::register('admin.cuisines.edit', function ($breadcrumbs, $cuisine) {
    $breadcrumbs->parent('admin.cuisines.index');
    $breadcrumbs->push($cuisine->title, route('admin.cuisines.edit', $cuisine));
});

// Админпанель > Кухни > [Cuisine] > Удалить
Breadcrumbs::register('admin.cuisines.delete', function ($breadcrumbs, $cuisine) {
    $breadcrumbs->parent('admin.cuisines.edit', $cuisine);
    $breadcrumbs->push('Удалить', route('admin.cuisines.delete', $cuisine));
});



// Админпанель > Категории
Breadcrumbs::register('admin.categories.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Категории', route('admin.categories.index'));
});

// Админпанель > Категории > Новая запись
Breadcrumbs::register('admin.categories.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.categories.index');
    $breadcrumbs->push('Новая запись', route('admin.categories.create'));
});

// Админпанель > Категории > [Category]
Breadcrumbs::register('admin.categories.edit', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('admin.categories.index');
    $breadcrumbs->push($category->title, route('admin.categories.edit', $category));
});

// Админпанель > Категории > [Category] > Удалить
Breadcrumbs::register('admin.categories.delete', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('admin.categories.edit', $category);
    $breadcrumbs->push('Удалить', route('admin.categories.delete', $category));
});



// Админпанель > Блюда
Breadcrumbs::register('admin.dishes.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Блюда', route('admin.dishes.index'));
});

// Админпанель > Блюда > Новая запись
Breadcrumbs::register('admin.dishes.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dishes.index');
    $breadcrumbs->push('Новая запись', route('admin.dishes.create'));
});

// Админпанель > Блюда > [Dish]
Breadcrumbs::register('admin.dishes.edit', function ($breadcrumbs, $dish) {
    $breadcrumbs->parent('admin.dishes.index');
    $breadcrumbs->push($dish->title, route('admin.dishes.edit', $dish));
});

// Админпанель > Блюда > [Dish] > Удалить
Breadcrumbs::register('admin.dishes.delete', function ($breadcrumbs, $dish) {
    $breadcrumbs->parent('admin.dishes.edit', $dish);
    $breadcrumbs->push('Удалить', route('admin.dishes.delete', $dish));
});



// Админпанель > Повара
Breadcrumbs::register('admin.cooks.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Повара', route('admin.cooks.index'));
});

// Админпанель > Повара > Новая запись
Breadcrumbs::register('admin.cooks.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.cooks.index');
    $breadcrumbs->push('Новая запись', route('admin.cooks.create'));
});

// Админпанель > Повара > [Cook]
Breadcrumbs::register('admin.cooks.edit', function ($breadcrumbs, $cook) {
    $breadcrumbs->parent('admin.cooks.index');
    $breadcrumbs->push($cook->full_name, route('admin.cooks.edit', $cook));
});

// Админпанель > Повара > [Cook] > Удалить
Breadcrumbs::register('admin.cooks.delete', function ($breadcrumbs, $cook) {
    $breadcrumbs->parent('admin.cooks.edit', $cook);
    $breadcrumbs->push('Удалить', route('admin.cooks.delete', $cook));
});



// Админпанель > Заказы
Breadcrumbs::register('admin.orders.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Заказы', route('admin.orders.index'));
});

// Админпанель > Заказы > Новая запись
Breadcrumbs::register('admin.orders.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.orders.index');
    $breadcrumbs->push('Новая запись', route('admin.orders.create'));
});

// Админпанель > Заказы > [Order]
Breadcrumbs::register('admin.orders.edit', function ($breadcrumbs, $order) {
    $breadcrumbs->parent('admin.orders.index');
    $breadcrumbs->push($order->id, route('admin.orders.edit', $order));
});

// Админпанель > Заказы > [Order] > Удалить
Breadcrumbs::register('admin.orders.delete', function ($breadcrumbs, $order) {
    $breadcrumbs->parent('admin.orders.edit', $order);
    $breadcrumbs->push('Удалить', route('admin.orders.delete', $order));
});



// Админпанель > Отзывы
Breadcrumbs::register('admin.reviews.index', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.index');
	$breadcrumbs->push('Отзывы', route('admin.reviews.index'));
});

// Админпанель > Отзывы > Новая запись
Breadcrumbs::register('admin.reviews.create', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.reviews.index');
	$breadcrumbs->push('Новая запись', route('admin.reviews.create'));
});

// Админпанель > Отзывы > [Review]
Breadcrumbs::register('admin.reviews.edit', function ($breadcrumbs, $review) {
	$breadcrumbs->parent('admin.reviews.index');
	$breadcrumbs->push($review->id, route('admin.reviews.edit', $review));
});

// Админпанель > Отзывы > [Review] > Удалить
Breadcrumbs::register('admin.reviews.delete', function ($breadcrumbs, $review) {
	$breadcrumbs->parent('admin.reviews.edit', $review);
	$breadcrumbs->push('Удалить', route('admin.reviews.delete', $review));
});