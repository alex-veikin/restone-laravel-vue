<?php

use Illuminate\Http\Request;

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post( '/login', 'Api\AuthController@login' );
Route::post( '/logout', 'Api\AuthController@logout' );
Route::post( '/register', 'Api\AuthController@register' );

Route::group( [ 'prefix' => 'user' ], function () {
    Route::get( '/', 'Api\AuthController@getUser' );
    Route::post( '/edit/profile', 'Api\UserController@updateProfile' );
    Route::post( '/edit/password', 'Api\UserController@updatePassword' );
    Route::post( '/edit/avatar', 'Api\UserController@updateAvatar' );
    Route::post( '/edit/contacts', 'Api\UserController@updateContacts' );
} );

Route::get( '/index', 'Api\AppController@index' );
Route::get( '/page/{alias}', 'Api\AppController@page' );
Route::get( '/cuisines', 'Api\CuisineController@index' );
Route::get( '/cuisines/{alias}', 'Api\CuisineController@show' );
Route::get( '/categories', 'Api\CategoryController@index' );
Route::get( '/categories/{alias}', 'Api\CategoryController@show' );
Route::get( '/dishes', 'Api\DishController@index' );
Route::get( '/dishes/{id}', 'Api\DishController@show' );
Route::get( '/reviews', 'Api\ReviewController@index' );
Route::post( '/reviews', 'Api\ReviewController@store' );

Route::group( [ 'prefix' => 'cart' ], function () {
    Route::get( '/', 'Api\CartController@index' );
    Route::get( 'increase/{id}', 'Api\CartController@increaseItem' );
    Route::get( 'decrease/{id}', 'Api\CartController@decreaseItem' );
    Route::get( 'remove/{id}', 'Api\CartController@removeItem' );
    Route::get( 'clear', 'Api\CartController@clearCart' );
} );

Route::post( '/checkout', 'Api\OrderController@checkout' );