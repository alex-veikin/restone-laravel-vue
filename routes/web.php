<?php

// Admin Panel
Route::group([
    'prefix' => 'admin',
    'middleware' => [/*'auth',*/ 'admin'],
    'as' => 'admin.',
], function () {
    Route::get('/', 'AdminController@index')->name('index');

    Route::resource('users', 'UserController', ['except' => ['show']] );
	Route::get('users/{user}/delete', 'UserController@destroy')->name('users.delete');

    Route::resource('pages', 'PageController', ['except' => ['show']] );
	Route::get('pages/{page}/delete', 'PageController@destroy')->name('pages.delete');

	Route::resource('cuisines', 'CuisineController', ['except' => ['show']] );
    Route::get('cuisines/{cuisine}/delete', 'CuisineController@destroy')->name('cuisines.delete');

    Route::resource('categories', 'CategoryController', ['except' => ['show']] );
	Route::get('categories/{category}/delete', 'CategoryController@destroy')->name('categories.delete');

	Route::resource('dishes', 'DishController', ['except' => ['show']] );
    Route::get('dishes/{dish}/delete', 'DishController@destroy')->name('dishes.delete');

	Route::resource('cooks', 'CookController', ['except' => ['show']] );
    Route::get('cooks/{cook}/delete', 'CookController@destroy')->name('cooks.delete');

    Route::resource('orders', 'OrderController', ['only' => ['index', 'edit', 'update']] );

	Route::resource('reviews', 'ReviewController', ['except' => ['show']] );
	Route::get('reviews/{review}/delete', 'ReviewController@destroy')->name('reviews.delete');

    Route::group(['prefix' => 'menus', 'as' => 'menus.'], function () {
        Route::get('/', 'MenuController@index')->name('index');
	    Route::post('{menu}', 'MenuController@store')->name('store');
        Route::get('{menu}', 'MenuController@show')->name('show');
        Route::get('move/{link}/{direction}', 'MenuController@move')->name('move');
        Route::get('delete/{link}', 'MenuController@delete')->name('delete');
        Route::get('edit', 'MenuController@edit')->name('edit');
        Route::post('edit', 'MenuController@update')->name('update');
    });
});


Route::get('/activate/{token}', 'Api\AuthController@activate');


//Auth::routes();


// Vue routes
Route::get('/{vue_capture?}', function() {
	return view( 'vue');
})->where('vue_capture', '[\/\w\.-]*')->name('vue');