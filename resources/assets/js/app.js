require('./bootstrap');
require('./components');

import store from './store/store'
import router from './router'
import App from './components/App'
import VModal from 'vue-js-modal'
import VueTheMask from 'vue-the-mask'

Vue.use(VueTheMask);
Vue.use(VModal);

const app = new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
});
