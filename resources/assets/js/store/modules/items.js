export default {
    state: {
        cuisines: [],
        categories: [],
        cooks: [],
        menus: [],
        itemsLoading: true,
    },
    getters: {
        cuisines: state => state.cuisines,
        categories: state => state.categories,
        cooks: state => state.cooks,
        menus: state => state.menus,
        itemsLoading: state => state.itemsLoading,
    },
    actions: {
        async getItems({commit, state}) {
            state.itemsLoading = true;

            await axios.get('/api/index')
                .then(res => commit('GET_ITEMS', res.data))
                .catch(err => console.log(err));

            state.itemsLoading = false;
        },
    },
    mutations: {
        GET_ITEMS(state, data) {
            state.cuisines = data.cuisines;
            state.categories = data.categories;
            state.cooks = data.cooks;
            state.menus = data.menus;
        },
    }
}