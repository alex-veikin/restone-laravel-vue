export default {
    state: {
        user: null,
        userLoading: true,
        userErrors: {},
        userProfileUpdated: false,
        userPasswordUpdated: false,
        userAvatarUpdated: false,
        userContactsUpdated: false,
        userEmailUpdated: false,
    },
    getters: {
        user: state => state.user,
        userLoading: state => state.userLoading,
        userErrors: state => state.userErrors,
        userProfileUpdated: state => state.userProfileUpdated,
        userPasswordUpdated: state => state.userPasswordUpdated,
        userAvatarUpdated: state => state.userAvatarUpdated,
        userContactsUpdated: state => state.userContactsUpdated,
        userEmailUpdated: state => state.userEmailUpdated,
    },
    actions: {
        async getUser({dispatch, state}) {
            state.userLoading = true;

            await dispatch('refreshUser');

            state.userLoading = false;
        },
        refreshUser({commit}) {
            return axios.get('/api/user')
                .then(res => commit('GET_USER', res.data))
                .catch(err => console.log(err));
        },
        updateUserProfile({commit}, form) {
            axios.post('/api/user/edit/profile', form)
                .then(res => commit('UPDATE_USER_PROFILE', {data: res.data, form}))
                .catch(err => console.log(err));
        },
        updateUserPassword({commit}, form) {
            axios.post('/api/user/edit/password', form)
                .then(res => commit('UPDATE_USER_PASSWORD', res.data))
                .catch(err => console.log(err));
        },
        updateUserAvatar({commit}, form) {
            axios.post('/api/user/edit/avatar', form)
                .then(res => commit('UPDATE_USER_AVATAR', res.data))
                .catch(err => console.log(err));
        },
        updateUserContacts({commit}, form) {
            axios.post('/api/user/edit/contacts', form)
                .then(res => commit('UPDATE_USER_CONTACTS', {data: res.data, form}))
                .catch(err => console.log(err));
        },
        clearUserErrors({state}) {
            state.userErrors = {};
        }
    },
    mutations: {
        GET_USER(state, data) {
            if (data.user) {
                state.user = data.user;

                state.user.orders.forEach((order) => {
                    order.cart = JSON.parse(order.cart)
                })
            }
        },
        UPDATE_USER_PROFILE(state, payload) {
            state.userErrors = payload.data.errors ? payload.data.errors : [];

            if (payload.data.success) {
                state.user.first_name = payload.form.first_name;
                state.user.last_name = payload.form.last_name;
                state.userProfileUpdated = true;

                setTimeout(() => state.userProfileUpdated = false, 3000)
            }
        },
        UPDATE_USER_PASSWORD(state, data) {
            state.userErrors = data.errors ? data.errors : [];

            if (data.success) {
                state.userPasswordUpdated = true;

                setTimeout(() => state.userPasswordUpdated = false, 3000)
            }
        },
        UPDATE_USER_AVATAR(state, data) {
            state.userErrors = data.errors ? data.errors : [];

            if (data.success) {
                state.user.profile.avatar = data.avatar;
                state.userAvatarUpdated = true;

                setTimeout(() => state.userAvatarUpdated = false, 3000)
            }
        },
        UPDATE_USER_CONTACTS(state, payload) {
            state.userErrors = payload.data.errors ? payload.data.errors : [];

            if (payload.data.success) {
                state.user.profile.phone = payload.form.phone;
                state.user.profile.address = payload.form.address;
                state.userContactsUpdated = true;

                setTimeout(() => state.userContactsUpdated = false, 3000)
            }

            if (payload.data.new_email) {
                state.user.new_email = payload.data.new_email;
            }
        },
        LOGOUT(state) {
            state.user = null;
        },
        PUSH_USER_ORDER(state, order) {
            state.user.orders.push(order)
        }
    }
}