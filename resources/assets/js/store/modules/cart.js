import router from '../../router'

export default {
    state: {
        cart: {
            items: [],
            totalQty: 0,
            totalPrice: false,
        },
        cartLoading: true,
        successOrder: null,
        paidOrder: null,
    },
    getters: {
        cartItems: state => state.cart.items,
        cartTotalQty: state => state.cart.totalQty,
        cartTotalPrice: state => {
            if (state.cart.totalPrice) {
                return parseFloat(state.cart.totalPrice).toFixed(2)
            } else {
                return state.cart.totalPrice;
            }
        },
        cartLoading: state => state.cartLoading,
        successOrder: state => state.successOrder,
        paidOrder: state => state.paidOrder,
    },
    actions: {
        async getCart({commit, state}) {
            state.cartLoading = true;

            await axios.get('/api/cart')
                .then(res => commit('GET_CART', res.data))
                .catch(err => {
                    console.log(err);
                    alert("Не удалось получить корзину");
                });

            state.cartLoading = false;
        },
        increaseCartItem({commit}, id) {
            axios.get('/api/cart/increase/' + id)
                .then(res => commit('GET_CART', res.data))
                .catch(err => console.log(err));
        },
        decreaseCartItem({commit}, id) {
            axios.get('/api/cart/decrease/' + id)
                .then(res => commit('GET_CART', res.data))
                .catch(err => console.log(err));
        },
        deleteCartItem({commit}, id) {
            if (confirm('Вы уверены что хотите удалить пункт?')) {
                axios.get('/api/cart/remove/' + id)
                    .then(res => commit('GET_CART', res.data))
                    .catch(err => console.log(err));
            }
        },
        clearCart({commit}) {
            if (confirm('Вы уверены что хотите очистить корзину?')) {
                axios.get('/api/cart/clear')
                    .then(res => commit('GET_CART', res.data))
                    .catch(err => console.log(err));
            }
        },
        payment({commit}, form) {
            axios.get('/api/checkout', form)
                .then(res => {
                    commit('SUCCESS_ORDER', res.data);

                    if (res.data.order) {
                        let order = res.data.order;
                        order.cart = JSON.parse(order.cart);
                        commit('PUSH_USER_ORDER', order, {module: 'user'})
                    }
                })
                .catch(err => {
                    console.log(err);
                    alert("Не удалось сохранить заказ");
                });
        },
        setSuccessOrder({commit}, data) {
            commit('SET_SUCCESS_ORDER', data)
        }
    },
    mutations: {
        GET_CART(state, data) {
            if (data.cart) {
                state.cart = data.cart;
                state.successOrder = null;
                state.paidOrder = null;
            } else {
                state.cart = {
                    items: [],
                    totalQty: 0,
                    totalPrice: false
                }
            }
        },
        SUCCESS_ORDER(state, data) {
            if (data.success) {
                state.cart = {
                    items: [],
                    totalQty: 0,
                    totalPrice: false,
                };
            }

            state.successOrder = data.success;
            state.paidOrder = data.paid;

            if (state.successOrder) {
                setTimeout(function () {
                    router.push({name: 'userOrders'});
                }, 3000);
            }
        },
        SET_SUCCESS_ORDER(state, data) {
            state.successOrder = data;
        }
    }
}