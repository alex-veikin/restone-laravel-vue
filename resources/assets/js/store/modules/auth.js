export default {
    namespace: 'auth',
    state: {
        authErrors: {},
        errorAuthentication: null,
        errorVerification: null,
        registered: false
    },
    getters: {
        authErrors: state => state.authErrors,
        errorAuthentication: state => state.errorAuthentication,
        errorVerification: state => state.errorVerification,
        registered: state => state.registered
    },
    actions: {
        login({commit}, form) {
            axios.post('/api/login', form)
                .then(async res => {
                    await commit('LOGIN', res.data);

                    if (res.data.verified === 1) {
                        axios.get('/api/user')
                            .then(res => commit('GET_USER', res.data, {module: 'user'}))
                            .catch(err => console.log(err));
                    }
                })
                .catch(err => console.log(err));
        },
        register({commit}, form) {
            axios.post('/api/register', form)
                .then(res => commit('REGISTER', res.data))
                .catch(err => console.log(err));
        },
        logout({commit}) {
            axios.post('/api/logout')
                .then(res => commit('LOGOUT', res.data, {module: 'user'}))
                .catch(err => console.log(err));
        },
        clearAuthErrors({state}) {
            state.authErrors = {};
        }
    },
    mutations: {
        LOGIN(state, data) {
            state.authErrors = data.errors ? data.errors : [];
            state.errorAuthentication = data.hasOwnProperty('errorAuth') ? data.errorAuth : null;
            state.errorVerification = data.hasOwnProperty('verified') ? data.verified : null;
        },
        REGISTER(state, data) {
            state.authErrors = data.errors ? data.errors : [];

            if (data.registered) {
                state.registered = data.registered;
            }
        }
    }
}