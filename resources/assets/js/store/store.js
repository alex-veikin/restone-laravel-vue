window.Vue = require('vue');
window.Vuex = require('vuex');
Vue.use(Vuex);

import cart from './modules/cart';
import auth from './modules/auth';
import user from './modules/user';
import items from './modules/items';

export default new Vuex.Store({
    modules: {
        cart,
        auth,
        user,
        items
    },
    state: {},
    mutations: {},
    actions: {}
});