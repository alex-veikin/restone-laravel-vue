window.Vue = require('vue');
window.VueRouter = require('vue-router');
window.VueMeta = require('vue-meta');

Vue.use(VueRouter);
Vue.use(VueMeta);

export default new VueRouter({
    mode: 'history',
    base: window.location.pathName,
    routes: [
        {
            path: '/',
            name: 'home',
            component: require('./components/site/Home')
        },
        {
            path: '/user',
            component: require('./components/site/user/User'),
            name: 'user',
            redirect: {name: 'userProfile'},
            children: [
                {
                    path: 'profile',
                    name: 'userProfile',
                    component: require('./components/site/user/UserProfile')
                },
                {
                    path: 'contacts',
                    name: 'userContacts',
                    component: require('./components/site/user/UserContacts')
                },
                {
                    path: 'orders',
                    name: 'userOrders',
                    component: require('./components/site/user/UserOrders')
                },
                {
                    path: 'orders/:id',
                    name: 'userOrder',
                    component: require('./components/site/user/UserOrder')
                },
            ]
        },
        {
            path: '/cart',
            name: 'cart',
            component: require('./components/site/Cart')
        },
        {
            path: '/checkout',
            name: 'checkout',
            component: require('./components/site/Checkout')
        },
        {
            path: '/cuisines',
            name: 'cuisines',
            component: require('./components/site/Cuisines')
        },
        {
            path: '/cuisines/:alias',
            name: 'cuisine',
            component: require('./components/site/Cuisine')
        },
        {
            path: '/categories',
            name: 'categories',
            component: require('./components/site/Categories')
        },
        {
            path: '/categories/:alias',
            name: 'category',
            component: require('./components/site/Category')
        },
        {
            path: '/dishes',
            name: 'dishes',
            component: require('./components/site/Dishes')
        },
        {
            path: '/dishes/:id',
            name: 'dish',
            component: require('./components/site/Dish')
        },
        {
            path: '/cooks',
            name: 'cooks',
            component: require('./components/site/Cooks')
        },
        {
            path: '/reviews',
            name: 'reviews',
            component: require('./components/site/Reviews')
        },
        {
            path: '/about',
            name: 'about',
            component: require('./components/site/About')
        },
        {
            path: '/shipping',
            name: 'shipping',
            component: require('./components/site/Page')
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: require('./components/site/Page')
        },
        {
            path: '/404',
            name: '404',
            component: require('./components/site/404')
        },
        {
            path: '/:page',
            component: require('./components/site/Page')
        },
    ],
    scrollBehavior (to, from, savedPosition) {
        if (to.hash) {
            return { selector: to.hash }
        } else if (savedPosition) {
            return savedPosition;
        } else {
            return { x: 0, y: 0 }
        }
    }
});
