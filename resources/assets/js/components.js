// Vue.component('passport-clients',require('./components/passport/Clients.vue'));
// Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue'));
// Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue'));

Vue.component('dish-items', require('./components/site/layouts/DishItems'));
Vue.component('loading', require('./components/site/layouts/Loading'));
Vue.component('pagination', require('./components/site/layouts/Pagination'));
Vue.component('back-forward', require('./components/site/layouts/BackForward'));
Vue.component('image-frame', require('./components/site/layouts/ImageFrame'));
