require('./bootstrap');

$(function () {

    let admin_sidebar = $('.admin_sidebar'),
        admin_sidebar__button = $('.admin_sidebar__button');

// Admin sidebar open-close
    admin_sidebar__button.on('click', function () {
        admin_sidebar.toggleClass('show');

        if (admin_sidebar.hasClass('show')) {
            admin_sidebar__button.find('.open').hide();
            admin_sidebar__button.find('.close').show();
        } else {
            admin_sidebar__button.find('.open').show();
            admin_sidebar__button.find('.close').hide();
        }
    });


    let btn = $('.add-nav-item .buttons button');
    let group_type = $('form .group');

// Choose type of the nav link
    btn.on('click', function () {
        // get selected type
        let type = $(this).find('label').attr('for');

        // add style for selected button
        $(this).addClass('active');
        btn.not(this).removeClass('active');

        // show form of selected type
        group_type.each(function () {
            if ($(this).hasClass(type)) {
                $(this).addClass('active')
            } else {
                $(this).removeClass('active')
            }
        });
    });

// Hide the record entry form on load window
    $('.overlay').addClass('hide');

// Show form
    $('.add-item-btn').on('click', function () {
        $('.overlay').removeClass('hide').addClass('show');
    });

// Hide form
    $('.close-btn').on('click', function () {
        $('.overlay').removeClass('show').addClass('hide');
    });


// confirm to delete menu item
    $('.delete-item-btn').on('click', function (e) {
        if (!confirm('Удалить пункт меню?')) {
            e.preventDefault();
        }
    });


    if ($('#ckeditor').length) {
        CKEDITOR.replace('ckeditor');
    }

});
