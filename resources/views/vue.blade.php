<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8"/>
    <base href="/">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Ресторан RestOne | Гомель</title>
    <meta name="keywords" content="ресторан RestOne, меню ресторана, рестораны гомеля, кухни, стран, мира, блюда, ресторан в гомеле">
    <meta name="description" content="Ресторан лучших блюд мировой кухни с доставкой на дом в городе Гомеле.">

    <link rel="icon" href="{{ asset('favicon.ico') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        {{--window.Laravel = {!! json_encode([--}}
        {{--'csrfToken' => csrf_token(),--}}
        {{--*/--}}
        {{--]) !!};--}}
    </script>
</head>
<body>

<div id="app"></div>

<script src="{{ asset('js/manifest.js') }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>