<ul>
    @foreach ($current as $link)
        @if($link->type == 'url')
            <li>
                <a href='{{ url($link->url) }}'>{{ $link->url_title }}</a>
            </li>
        @elseif($link->type == 'page')
            <li class='{{ (Request::path() == $link->page->alias ) ? 'link-active' : '' }}'>
                <a href='{{ url($link->page->alias) }}'>{{ $link->page->title }}</a>
            </li>
        @elseif($link->type == 'submenu')
            <li>
                <button class="submenu-btn">
                    {{ $link->submenu_title }}
                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                </button>

                {!! view('layouts.top-menu', ['nav' => $nav, 'current' => $nav->where('parent_id', $link->id)->all()]) !!}
            </li>
        @endif

        @if( $link->parent_id == 0 && $link->weight == 2 )
            <div class="header__nav-logo">
                <a href="/">
                    <img src="{{ asset('img/logo.png') }}" alt="Rest ONE">
                </a>
            </div>
        @endif
    @endforeach
</ul>