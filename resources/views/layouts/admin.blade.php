<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle')Rest One</title>
    <meta name="description" content="@yield('pageDescription')">

    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

</head>
<body>

<section>
    <aside class="admin_sidebar">
        <div class="admin_sidebar__button">
            <div class="open"><i class="fa fa-bars" aria-hidden="true"></i></div>
            <div class="close"><i class="fa fa-times" aria-hidden="true"></i></div>
        </div>
        <ul>
            @foreach( \App\AdminNavLink::all() as $link )
                @if(Request::segment(2) == $link->segment)
                    <li class="active">{{ link_to($link->uri, $link->title) }}</li>
                @else
                    <li>{{ link_to($link->uri, $link->title) }}</li>
                @endif
            @endforeach
        </ul>
    </aside>

    <div class="main">
        <header>
            <div class="top">
                <h1>Rest <span>One</span></h1>
                {{--{!! link_to_route('user.index', auth::user()->login) !!}--}}
                {!! link_to('/', 'Перейти на сайт') !!}
                {{--{!! link_to('/logout', 'Выйти', [], ['onclick' => 'event.preventDefault(); document.getElementById("logout-form").submit();']) !!}--}}
                {{--{!! Form::open(['route'=>'logout', 'id'=>'logout-form', 'style'=>'display:none']) !!}--}}
                {{--{!! Form::close() !!}--}}
            </div>

            <div class="bottom">
                @yield('breadcrumb')

                @yield('new-item')
            </div>
        </header>

        <div class="content">
            @if(session()->has('success'))
                <p class="success">{{ session('success') }}</p>
            @endif

            @yield('content')
        </div>
    </div>
</section>

<script src="{{ asset('js/manifest.js') }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
<script src="https://cdn.ckeditor.com/4.8.0/full-all/ckeditor.js"></script>
{{--<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>--}}

</body>
</html>
