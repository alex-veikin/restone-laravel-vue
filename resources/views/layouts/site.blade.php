<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8"/>
    <base href="/">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <meta property="og:image" content="{{ asset('img/og-image.jpg') }}">
    <link rel="icon" href="{{ asset('img/favicon/favicon.ico') }}">
    <link rel="apple-touch-icon" size="180x180" href="{{ asset('img/favicon/apple-touch-icon-180x180.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle')Rest One</title>
    <meta name="description" content="@yield('pageDescription')">

    @include('layouts.styles')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>

@include('layouts.header')

@yield('content')

@include('layouts.footer')

@include('layouts.scripts')

</body>
</html>