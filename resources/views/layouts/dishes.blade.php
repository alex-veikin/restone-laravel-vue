<div class="dish__items">
    @foreach ( $dishes as $dish )
        <div class="item">
            <div class="image">
                <div class="links">
                    {{ link_to_route('cuisines.show', $dish->cuisine->title.' кухня', $dish->cuisine->alias) }}
                    {{ link_to_route('categories.show', $dish->category->title, $dish->category->alias) }}
                </div>

                <a href="{{ route('dishes.show', [$dish]) }}">
                    <div class="overlay">
                        <p>Посмотреть описание</p>
                    </div>

                    <div class="title">{{ $dish->title }}</div>

                    <img src="{{ asset('img/dishes/' . $dish->image) }}" alt="{{ $dish->title }}">
                </a>
            </div>

            <div class="bottom">
                <div class="price">{{ $dish->price }} руб.</div>
                <a href="{{ route('cart.increase', $dish) }}" class="buy">В корзину</a>
            </div>
        </div>
    @endforeach
</div>