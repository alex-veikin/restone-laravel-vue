<header class="header">
    <div class="header__top">
        <div class="wrapper">
            <div class="header__top-contacts">
                <div class="phone"><span></span>+375 (29) 111-11-11</div>
                <div class="email"><span></span>info@rest-one.by</div>
            </div>

            @if( Auth::check() )
                <span class="user-name">
                    @if(Auth::user()->role === 'admin')
                        {{ link_to_route('admin.index', 'Админпанель') }}
                    @else
                        {!! link_to_route('user.index', Auth::user()->first_name, [], ['class'=>'user-name-link']) !!}
                    @endif

                    {!! link_to_route('logout', 'Выйти', [], ['onclick' => 'event.preventDefault(); document.getElementById("logout-form").submit();']) !!}
                    {!! Form::open(['route'=>'logout', 'id'=>'logout-form', 'style'=>'display:none']) !!}
                    {!! Form::close() !!}
                </span>
            @else
                <span class="login-open auth">Авторизация <i class="fa fa-angle-down" aria-hidden="true"></i></span>

                <div class="header__top-auth">
                    {!! Form::open(['route'=>'login']) !!}
                    {!! Form::text('login', '', ['placeholder' => 'логин']) !!}
                    {!! Form::password('password', ['placeholder' => 'пароль']) !!}
                    {!! Form::submit('Вход', ['class' => 'submit']) !!}
                    <label class="remember">
                        {!! Form::checkbox('remember', old('remember') ? 'checked' : '', true, ['title'=>'Запомнить меня']) !!}
                        <span>Запомнить</span>
                    </label>
                    <span class="link_to_reg">
                        <span>или</span>
                        {!! link_to_route('register', 'Регистрация', [], []) !!}
                    </span>
                    {!! Form::close() !!}
                </div>
            @endif
        </div>
    </div>
    <div class="wrapper">
        <nav class="header__nav">
            {!! view('layouts.top-menu', ['nav' => $top_nav, 'current' => $top_nav->where('parent_id', 0)->all()]) !!}
        </nav>

        <nav class="header__nav-hidden">
            <button class="nav-open-btn"><i class="fa fa-bars" aria-hidden="true"></i></button>
            <button class="nav-close-btn"><i class="fa fa-times" aria-hidden="true"></i></button>

            <div class="side-nav">
                {!! view('layouts.side-menu', ['nav' => $side_nav, 'current' => $side_nav->where('parent_id', 0)->all()]) !!}
            </div>
        </nav>

        <a href="/cart" class="cart-btn">
            <div class="cart-btn__qty">{{ session('cart')->totalQty ?? 0 }}</div>
            @if(session()->has('cart'))
                <div class="cart-btn__price">{{ session('cart')->totalPrice }} руб</div>
            @endif
            <img src="{{ asset('img/cart.png') }}" alt="">
        </a>
    </div>
</header>