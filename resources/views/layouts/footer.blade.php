<footer class="footer">
    <div class="wrapper">
        <div class="footer__top">
            <div class="footer__contacts">
                <div class="footer__contacts-item">
                    <p>Адрес:</p>
                    <p>246000, Беларусь, г. Гомель, проспект Победы, 18</p>
                </div>
                <div class="footer__contacts-item">
                    <p>Контакты:</p>
                    <p>info@rest-one.by</p>
                    <p>+375 (44) 111 11 11</p>
                    <p>+375 (29) 111 11 11</p>
                    <p>+375 (25) 111 11 11</p>
                </div>
                <div class="footer__contacts-map">
                    <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A25c81b57ea6d49bd3589cca2bfbd96585f726383ff89cdb119b4f74d6761f223&amp;source=constructor"
                            width="500" height="400" frameborder="0"></iframe>
                </div>
            </div>
            <nav class="footer__nav">
                <div class="footer__nav-nav">
                    <p>Навигация</p>
                    <ul>
                        <li><a href="">Главная</a></li>
                        <li><a href="">Меню</a></li>
                        <li><a href="">Кухни</a></li>
                        <li><a href="">Корзина</a></li>
                        <li><a href="">Галерея</a></li>
                        <li><a href="">Статьи</a></li>
                        <li><a href="">Отзывы</a></li>
                        <li><a href="">О нас</a></li>
                    </ul>
                </div>
                <div class="footer__nav-cooks">
                    <p>Наши повара</p>
                    <ul>
                        <li><a href="">Джейми Оливер</a></li>
                        <li><a href="">Ина Гартен</a></li>
                        <li><a href="">Тодд Инглиш</a></li>
                        <li><a href="">Рэйчел Рэй</a></li>
                        <li><a href="">Марио Батали</a></li>
                    </ul>
                </div>
                <div class="footer__nav-info">
                    <p>Информация</p>
                    <ul>
                        <li><a href="">Как заказать</a></li>
                        <li><a href="">Как оплатить</a></li>
                        <li><a href="">Партнеры</a></li>
                        <li><a href="">Ссылки</a></li>
                        <li><a href="">Контакты</a></li>
                    </ul>
                </div>
                <div class="footer__social">
                    <div class="footer__social-text">Следите за нами в социальных сетях:</div>
                    <div class="footer__social-icons">
                        <a class="fb" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a class="vk" href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                        <a class="ok" href="#"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                        <a class="tw" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a class="in" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a class="gp" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    </div>
                </div>
            </nav>
        </div>
        <div class="footer__bottom">
            <div class="copyright">2018 &copy; Все права защищены.</div>
            <div class="footer__payment">
                <img src="{{ asset('img/Visa.png') }}" alt="visa">
                <img src="{{ asset('img/MasterCard.png') }}" alt="mastercard">
                <img src="{{ asset('img/Maestro.png') }}" alt="maestro">
                <img src="{{ asset('img/easypay.png') }}" alt="easypay">
                <img src="{{ asset('img/webmoney.png') }}" alt="webmoney">
            </div>
        </div>
    </div>
</footer>