@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('new-item', link_to_route('admin.categories.create', 'Новая запись',[] , ['class'=>'add']))

@section('content')
    <table>
        <tr>
            <th>ID</th>
            <th>Заголовок</th>
        </tr>
        @foreach($categories as $category)
            <tr>
                <td class="small">
                    {{ link_to_route('admin.categories.edit', $category->id, [$category]) }}
                </td>
                <td>{{ $category->title }}</td>
            </tr>
        @endforeach
    </table>
@endsection