@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $cook))

@section('content')
    {!! Form::open(['route'=>['admin.cooks.destroy', $cook], 'method'=>'DELETE']) !!}
    <div class="group">
        {!! Form::submit('Удалить пункт', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection