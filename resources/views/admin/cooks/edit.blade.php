@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $cook))

@section('new-item', link_to_route('admin.cooks.delete', 'Удалить запись',[$cook] , ['class'=>'delete']))

@section('content')
    {!! Form::model($cook, [
                        'route'=>['admin.cooks.update', $cook],
                        'enctype'=>'multipart/form-data',
                        'method'=>'PATCH'
                    ]) !!}
    <div class="group">
        {!! Form::label('Номер') !!}
        {!! Form::selectRange('weight', 1, \App\Cook::all()->count(), $cook->weight) !!}
        @if($errors->has('weight'))
            <p class="error">{{ $errors->first('weight') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Alias') !!}
        {!! Form::text('alias', null, ['placeholder'=>'Псевдоним']) !!}
        @if($errors->has('alias'))
            <p class="error">{{ $errors->first('alias') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Имя и фамилия') !!}
        {!! Form::text('full_name', null, ['placeholder'=>'Имя и фамилия']) !!}
        @if($errors->has('full_name'))
            <p class="error">{{ $errors->first('full_name') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Должность') !!}
        {!! Form::text('title', null, ['placeholder'=>'Заголовок']) !!}
        @if($errors->has('title'))
            <p class="error">{{ $errors->first('title') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Описание') !!}
        {!! Form::textarea('description', null, ['placeholder'=>'Описание', 'id'=>'ckeditor']) !!}
        @if($errors->has('description'))
            <p class="error">{{ $errors->first('description') }}</p>
        @endif
    </div>
    <div class="group">
        <img src="{{ asset('/img/cooks/'.$cook->image) }}" alt="{{ $cook->full_name }}">
    </div>
    <div class="group">
        {!! Form::label('Изменить изображение') !!}
        {!! Form::file('file') !!}
        @if($errors->has('file'))
            <p class="error">{{ $errors->first('file') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::submit('Сохранить изменения', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection