@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('content')
    {!! Form::open(['route'=>'admin.cooks.store', 'enctype'=>'multipart/form-data']) !!}
    <div class="group">
        {!! Form::label('Alias') !!}
        {!! Form::text('alias', old('alias'), ['placeholder'=>'Псевдоним']) !!}
        @if($errors->has('alias'))
            <p class="error">{{ $errors->first('alias') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Имя и фамилия') !!}
        {!! Form::text('full_name', old('full_name'), ['placeholder'=>'Имя и фамилия']) !!}
        @if($errors->has('full_name'))
            <p class="error">{{ $errors->first('full_name') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Должность') !!}
        {!! Form::text('title', old('title'), ['placeholder'=>'Заголовок']) !!}
        @if($errors->has('title'))
            <p class="error">{{ $errors->first('title') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Описание') !!}
        {!! Form::textarea('description', old('description'), ['placeholder'=>'Описание', 'id'=>'ckeditor']) !!}
        @if($errors->has('description'))
            <p class="error">{{ $errors->first('description') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Изображение') !!}
        {!! Form::file('file') !!}
        @if($errors->has('file'))
            <p class="error">{{ $errors->first('file') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::submit('Сохранить', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection