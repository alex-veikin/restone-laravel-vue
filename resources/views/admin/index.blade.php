@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('content')
    <div class="info-items">
        <div class="info-item">
            <a href="{{ route('admin.users.index') }}">
                <div class="title">Пользователи</div>
                <div class="info">{{ $users }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.pages.index') }}">
                <div class="title">Страницы</div>
                <div class="info">{{ $pages }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.menus.index') }}">
                <div class="title">Меню</div>
                <div class="info">{{ $menus }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.cuisines.index') }}">
                <div class="title">Кухни</div>
                <div class="info">{{ $cuisines }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.categories.index') }}">
                <div class="title">Категории</div>
                <div class="info">{{ $categories }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.dishes.index') }}">
                <div class="title">Блюда</div>
                <div class="info">{{ $dishes }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.cooks.index') }}">
                <div class="title">Повара</div>
                <div class="info">{{ $cooks }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.orders.index') }}">
                <div class="title">Заказы</div>
                <div class="info">
                    @if($ordersActive)
                        <span class="active">{{ $ordersActive }}</span>
                        <span>/</span>
                    @endif
                    <span class="inactive">{{ $orders }}</span>
                </div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.reviews.index') }}">
                <div class="title">Отзывы</div>
                <div class="info">
                    @if($reviewsActive)
                        <span class="active">{{ $reviewsActive }}</span>
                        <span>/</span>
                    @endif
                    <span class="inactive">{{ $reviews }}</span>
                </div>
            </a>
        </div>
    </div>
@endsection