@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $user))

@section('new-item', link_to_route('admin.users.delete', 'Удалить запись',[$user] , ['class'=>'delete']))

@section('content')
    {!! Form::model($user, ['route'=>['admin.users.update', $user], 'method'=>'PATCH']) !!}
    <div class="group">
        {!! Form::label('Права') !!}
        {!! Form::select('role', ['user'=>'user', 'admin'=>'admin'], null) !!}
        @if($errors->has('role'))
            <p class="error">{{ $errors->first('role') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Имя') !!}
        {!! Form::text('first_name', null, ['placeholder'=>'имя']) !!}
        @if($errors->has('first_name'))
            <p class="error">{{ $errors->first('first_name') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Фамилия') !!}
        {!! Form::text('last_name', null, ['placeholder'=>'Фамилия']) !!}
        @if($errors->has('last_name'))
            <p class="error">{{ $errors->first('last_name') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Логин') !!}
        {!! Form::text('login', null, ['placeholder'=>'Логин']) !!}
        @if($errors->has('login'))
            <p class="error">{{ $errors->first('login') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Email') !!}
        {!! Form::email('email', null, ['placeholder'=>'Email']) !!}
        @if($errors->has('email'))
            <p class="error">{{ $errors->first('email') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::submit('Изменить данные', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection