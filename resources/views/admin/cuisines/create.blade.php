@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('content')
    {!! Form::open(['route'=>'admin.cuisines.store', 'enctype'=>'multipart/form-data']) !!}
    <div class="group">
        {!! Form::label('Alias') !!}
        {!! Form::text('alias', old('alias'), ['placeholder'=>'Псевдоним']) !!}
        @if($errors->has('alias'))
            <p class="error">{{ $errors->first('alias') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Заголовок') !!}
        {!! Form::text('title', old('title'), ['placeholder'=>'Заголовок']) !!}
        @if($errors->has('title'))
            <p class="error">{{ $errors->first('title') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Описание') !!}
        {!! Form::textarea('description', old('description'), ['placeholder'=>'Описание', 'id'=>'ckeditor']) !!}
        @if($errors->has('description'))
            <p class="error">{{ $errors->first('description') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Изображение') !!}
        {!! Form::file('file', ['id'=>'cropper']) !!}
        @if($errors->has('file'))
            <p class="error">{{ $errors->first('file') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::submit('Сохранить', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection