@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('new-item', link_to_route('admin.cuisines.create', 'Новая запись',[] , ['class'=>'add']))

@section('content')
    <table>
        <tr>
            <th>ID</th>
            <th>Заголовок</th>
        </tr>
        @foreach($cuisines as $cuisine)
            <tr>
                <td class="small">
                    {{ link_to_route('admin.cuisines.edit', $cuisine->id, [$cuisine]) }}
                </td>
                <td>{{ $cuisine->title }} кухня</td>
            </tr>
        @endforeach
    </table>
@endsection