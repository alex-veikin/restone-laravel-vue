@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $order))

@section('content')
    <table>
        <tr>
            <th>№</th>
            <th>Блюдо</th>
            <th>Цена</th>
            <th>Кол-во</th>
            <th>Сумма</th>
        </tr>
        @foreach($cart->items as $dish)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $dish->item->title }}</td>
                <td>{{ $dish->item->price }} руб.</td>
                <td>{{ $dish->qty }}</td>
                <td>{{ $dish->price }} руб.</td>
            </tr>
        @endforeach
        <tr class="total">
            <td colspan="3">Итого:</td>
            <td>{{ $cart->totalQty }}</td>
            <td>{{ $cart->totalPrice }} руб.</td>
        </tr>
    </table>

    {!! Form::model($order, ['route'=>['admin.orders.update', $order], 'method'=>'PATCH']) !!}
    <div class="group">
        {!! Form::label('Неактивен:') !!}
        {!! Form::radio('status', 0, $order->details->status == 0, ['title'=>'Неактивен']) !!}
        {!! Form::label('Активен:') !!}
        {!! Form::radio('status', 1, $order->details->status == 1, ['title'=>'Активен']) !!}
    </div>
    <div class="group">
        {!! Form::submit('Сохранить изменения', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection