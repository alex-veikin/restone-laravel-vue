@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('content')
    <table>
        <tr>
            <th>ID</th>
            <th>Дата заказа</th>
            <th>Дата закрытия</th>
            <th>Кол-во товаров</th>
            <th>Сумма заказа</th>
            <th>Заказчик</th>
            <th>Статус</th>
        </tr>
        @foreach($orders as $order)
            <tr class="{{ ($order->details->status == 1) ? 'active' : '' }}">
                <td class="small">
                    {{ link_to_route('admin.orders.edit', $order->id, [$order]) }}
                </td>
                <td>{{ $order->created_at }}</td>
                <td>{{ $order->status == 0 ? $order->updated_at : '' }}</td>
                <td>{{ json_decode($order->cart)->totalQty }}</td>
                <td>{{ json_decode($order->cart)->totalPrice }}</td>
                <td>{{ $order->user->first_name . ' ' . $order->user->last_name }}</td>
                <td>{{ $order->details->status == 1 ? 'Активен' : 'Неактивен' }}</td>
            </tr>
        @endforeach
    </table>
@endsection