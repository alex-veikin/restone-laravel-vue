@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $review))

@section('content')
    {!! Form::open(['route'=>['admin.reviews.destroy', $review], 'method'=>'DELETE']) !!}
    <div class="group">
        {!! Form::submit('Удалить пункт', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection