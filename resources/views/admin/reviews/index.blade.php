@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('new-item', link_to_route('admin.reviews.create', 'Новая запись',[] , ['class'=>'add']))

@section('content')
    <table>
        <tr>
            <th>ID</th>
            <th>Пользователь</th>
            <th>Рейтинг</th>
            <th>Отзыв</th>
        </tr>
        @foreach($reviews as $review)
            <tr class="{{ ($review->status == 1) ? 'active' : '' }}">
                <td class="small">
                    {{ link_to_route('admin.reviews.edit', $review->id, [$review]) }}
                </td>
                <td>
                    {{ link_to_route('admin.users.edit', $review->user->first_name, [$review->user]) }}
                </td>
                <td>{{ $review->rating }}</td>
                <td>{{ $review->text }}</td>
            </tr>
        @endforeach
    </table>
@endsection