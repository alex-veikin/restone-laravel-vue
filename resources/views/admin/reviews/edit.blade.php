@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $review))

@section('new-item', link_to_route('admin.reviews.delete', 'Удалить запись',[$review] , ['class'=>'delete']))

@section('content')
    {!! Form::model($review, ['route'=>['admin.reviews.update', $review], 'method'=>'PATCH']) !!}
    <div class="group">
        {!! Form::label('Рейтинг') !!}
        <div class="rating">
            @foreach(range(1,5) as $val)
                <input id="{{ 'rating' . $val }}" type="radio" hidden name="rating"
                       value="{{ $val }}" {{ $val == $review->rating ? 'checked' : '' }}>
                <label for="{{ 'rating' . $val }}" class="fa fa-star">
                </label>
            @endforeach
        </div>
        @if($errors->has('rating'))
            <p class="error">{{ $errors->first('rating') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Отзыв') !!}
        {!! Form::textarea('text', old('text'), ['placeholder'=>'Отзыв', 'rows' => 5]) !!}
        @if($errors->has('text'))
            <p class="error">{{ $errors->first('text') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Показывать:') !!}
        {!! Form::radio('status', 0, $review->status == 0, ['title'=>'Неактивен']) !!}
        {!! Form::label('Не показывать:') !!}
        {!! Form::radio('status', 1, $review->status == 1, ['title'=>'Активен']) !!}
    </div>
    <div class="group">
        {!! Form::submit('Сохранить', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection