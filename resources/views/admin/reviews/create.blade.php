@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('content')
    {!! Form::open(['route'=>'admin.reviews.store']) !!}
    <div class="group">
        {!! Form::label('Рейтинг') !!}
        <div class="rating">
            @foreach(range(1,5) as $val)
                <input id="{{ 'rating' . $val }}" type="radio" hidden name="rating" value="{{ $val }}">
                <label for="{{ 'rating' . $val }}" class="fa fa-star">
                </label>
            @endforeach
        </div>
        @if($errors->has('rating'))
            <p class="error">{{ $errors->first('rating') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Отзыв') !!}
        {!! Form::textarea('text', null, ['placeholder'=>'Отзыв', 'rows' => 5]) !!}
        @if($errors->has('text'))
            <p class="error">{{ $errors->first('text') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Показывать:') !!}
        {!! Form::radio('status', 0, true, ['title'=>'Неактивен']) !!}
        {!! Form::label('Не показывать:') !!}
        {!! Form::radio('status', 1, null, ['title'=>'Активен']) !!}
    </div>
    <div class="group">
        {!! Form::submit('Сохранить', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection