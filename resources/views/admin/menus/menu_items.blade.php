<ul class="menu-list">
    @foreach($current as $link)
        <li>
            <div class="info">
                @if($link->type == 'url')
                    <span>Ссылка: </span>{{ $link->url_title }} (<i>{{ link_to($link->url) }})</i>
                @elseif($link->type == 'page')
                    <span>Страница: </span>{{ $link->page->title }}
                @elseif($link->type == 'submenu')
                    <span>Подменю: </span>{{ $link->submenu_title }}
                @endif

                <div class="buttons">
                    <a href="">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    <a href="{{ route('admin.menus.delete', ['id' => $link->id]) }}" class="delete-item-btn">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </a>
                    <div class="move">
                        @if($link->weight > 1)
                            <a href="{{ route('admin.menus.move', ['id' => $link->id, 'direction' => 'up']) }}">
                                <i class="fa fa-arrow-up" aria-hidden="true"></i>
                            </a>
                        @else
                            <a href="" class="not-active">
                                <i class="fa fa-arrow-up" aria-hidden="true"></i>
                            </a>
                        @endif

                        @if($link->weight < $current->count())
                            <a href="{{ route('admin.menus.move', ['id' => $link->id, 'direction' => 'down']) }}">
                                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                            </a>
                        @else
                            <a href="" class="not-active"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                        @endif
                    </div>
                </div>
            </div>

            @if($link->type == 'submenu')
                {!! view('admin.menus.menu_items', [
                    'nav' => $nav,
                    'items' => $items->sortBy('weight'),
                    'current' => $items->where('parent_id', $link->id)
                                       ->sortBy('weight')
                ]) !!}
            @endif
        </li>
    @endforeach
</ul>