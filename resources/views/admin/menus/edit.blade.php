@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $nav))

@section('content')
    {!! Form::open(['action'=>['AdminController@navUpdate', $nav]]) !!}

    @foreach($menu as $link)
        <div class="group">
            {!! Form::selectRange("links[{$link->id}][weight]", 1, $menu->count(), $link->weight, ['class'=>'small']) !!}
            {!! Form::select("links[{$link->id}][page_id]", $pages_select, $link->page_id) !!}
            {!! Form::select("links[{$link->id}][parent_id]", $parents_select, $link->parent_id) !!}
        </div>

        <div class="group">
            @if($errors->has("links.{$link->id}.weight"))
                <p class="error">{{ $errors->first("links.{$link->id}.weight") }}</p>
            @endif

            @if($errors->has("links.{$link->id}.page_id"))
                <p class="error">{{ $errors->first("links.{$link->id}.page_id") }}</p>
            @endif

            @if($errors->has("links.{$link->id}.parent_id"))
                <p class="error">{{ $errors->first("links.{$link->id}.parent_id") }}</p>
            @endif
        </div>
    @endforeach

    <div class="group">
        {!! Form::submit('Сохранить изменения', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection