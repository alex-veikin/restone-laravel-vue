@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

{{--@section('new-item')--}}
    {{--<a href="#" class="add add-item-btn">Новый пункт</a>--}}
{{--@endsection--}}

@section('content')
    <table>
        <tr>
            <th>ID</th>
            <th>alias</th>
            <th>Имя меню</th>
            <th>Кол-во элементов</th>
        </tr>
        @foreach($menus as $menu)
            <tr>
                <td class="small">
                    {{ link_to_route('admin.menus.show', $menu->id, [$menu->id]) }}
                </td>
                <td>{{ $menu->alias }}</td>
                <td>{{ $menu->title }}</td>
                <td>{{ $menu->items()->count() }}</td>
            </tr>
        @endforeach
    </table>
@endsection