<div class="overlay">
    <div class="add-nav-item">
        <button class="close-btn">
            <i class="fa fa-times" aria-hidden="true"></i>
        </button>

        <div class="buttons">
            <button class="active">
                <label for="url">Ссылка</label>
            </button>
            <button>
                <label for="page">Страница</label>
            </button>
            <button>
                <label for="submenu">Подменю</label>
            </button>
        </div>

        {!! Form::open(['route'=>['admin.menus.store', $menu->id]]) !!}
        {!! Form::radio('type', 'url', true, ['id'=>'url', 'hidden']) !!}
        {!! Form::radio('type', 'page', null, ['id'=>'page', 'hidden']) !!}
        {!! Form::radio('type', 'submenu', null, ['id'=>'submenu', 'hidden']) !!}
        <div class="group url active">
            {!! Form::label('Имя пункта') !!}
            {!! Form::text('url_title', null, ['placeholder'=>'имя пункта']) !!}
        </div>
        <div class="group url active">
            {!! Form::label('Ссылка') !!}
            {!! Form::text('url', 'http://', ['placeholder'=>'ссылка (http://...)']) !!}
        </div>
        <div class="group page">
            {!! Form::label('Страница') !!}
            {!! Form::select('page_id', $pages_select, null) !!}
        </div>
        <div class="group submenu">
            {!! Form::label('Подменю') !!}
            {!! Form::text('submenu_title', null, ['placeholder'=>'имя пункта']) !!}
        </div>
        <div class="group">
            {!! Form::label('Родительский пункт') !!}
            {!! Form::select('parent_id', $parents_select, null) !!}
        </div>
        <div class="group">
            {!! Form::submit('Добавить пункт меню', ['class' => 'submit']) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>