@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $menu))

@section('new-item')
    <a href="#" class="add add-item-btn">Новый пункт</a>
@endsection

@section('content')
    @if($errors->has('url_title'))
        <p class="error">{{ $errors->first('url_title') }}</p>
    @endif
    @if($errors->has('url'))
        <p class="error">{{ $errors->first('url') }}</p>
    @endif
    @if($errors->has('page_id'))
        <p class="error">{{ $errors->first('page_id') }}</p>
    @endif
    @if($errors->has('submenu_title'))
        <p class="error">{{ $errors->first('submenu_title') }}</p>
    @endif
    @if($errors->has('parent_id'))
        <p class="error">{{ $errors->first('parent_id') }}</p>
    @endif

    {!! view('admin.menus.menu_items', [
        'nav' => $menu->alias,
        'items' => $menu->items->sortBy('weight'),
        'current' => $menu->items->where('parent_id', 0)->sortBy('weight')
    ]) !!}

    {!! view('admin.menus.add_item_layout',
        compact('menu', 'pages_select', 'parents_select')
    ) !!}


@endsection