@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $dish))

@section('content')
    {!! Form::open(['route'=>['admin.dishes.destroy', $dish], 'method'=>'DELETE']) !!}
    <div class="group">
        {!! Form::submit('Удалить пункт', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection