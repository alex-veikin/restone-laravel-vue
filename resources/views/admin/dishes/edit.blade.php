@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $dish))

@section('new-item', link_to_route('admin.dishes.delete', 'Удалить запись',[$dish] , ['class'=>'delete']))

@section('content')
    {!! Form::model($dish, [
                        'route'=>['admin.dishes.update', $dish],
                        'enctype'=>'multipart/form-data',
                        'method'=>'PATCH'
                    ]) !!}
    <div class="group">
        {!! Form::label('Кухня') !!}
        {!! Form::select('cuisine_id', $cuisines, null) !!}
        @if($errors->has('cuisine_id'))
            <p class="error">{{ $errors->first('cuisine_id') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Раздел') !!}
        {!! Form::select('category_id', $categories, null) !!}
        @if($errors->has('category_id'))
            <p class="error">{{ $errors->first('category_id') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Заголовок') !!}
        {!! Form::text('title', old('title'), ['placeholder'=>'Заголовок']) !!}
        @if($errors->has('title'))
            <p class="error">{{ $errors->first('title') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Описание') !!}
        {!! Form::textarea('description', old('description'), ['placeholder'=>'Описание', 'id'=>'ckeditor']) !!}
        @if($errors->has('description'))
            <p class="error">{{ $errors->first('description') }}</p>
        @endif
    </div>
    <div class="group">
        <img src="{{ asset('/img/dishes/'.$dish->image) }}" alt="{{ $dish->title }}">
    </div>
    <div class="group">
        {!! Form::label('Изображение') !!}
        {!! Form::file('file') !!}
        @if($errors->has('file'))
            <p class="error">{{ $errors->first('file') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Цена') !!}
        {!! Form::text('price', old('price'), ['placeholder'=>'Цена']) !!}
        @if($errors->has('price'))
            <p class="error">{{ $errors->first('price') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::submit('Сохранить', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection