@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('content')
    {!! Form::open(['route'=>'admin.pages.store']) !!}
    <div class="group">
        {!! Form::label('Alias') !!}
        {!! Form::text('alias', old('alias'), ['placeholder'=>'Псевдоним']) !!}
        @if($errors->has('alias'))
            <p class="error">{{ $errors->first('alias') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Заголовок') !!}
        {!! Form::text('title', old('title'), ['placeholder'=>'Заголовок']) !!}
        @if($errors->has('title'))
            <p class="error">{{ $errors->first('title') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Описание') !!}
        {!! Form::textarea('description', old('description'), ['placeholder'=>'Описание', 'rows' => 3]) !!}
        @if($errors->has('description'))
            <p class="error">{{ $errors->first('description') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Контент') !!}
        {!! Form::textarea('content', old('content'), ['placeholder'=>'Контент', 'id'=>'ckeditor']) !!}
        @if($errors->has('content'))
            <p class="error">{{ $errors->first('content') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::submit('Создать страницу', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection