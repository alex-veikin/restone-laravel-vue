@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $page))

@section('new-item', link_to_route('admin.pages.delete', 'Удалить запись',[$page] , ['class'=>'delete']))

@section('content')
    {!! Form::model($page, ['route'=>['admin.pages.update', $page], 'method'=>'PATCH']) !!}
    <div class="group">
        {!! Form::label('Alias') !!}
        {!! Form::text('alias', null, ['placeholder'=>'Псевдоним']) !!}
        @if($errors->has('alias'))
            <p class="error">{{ $errors->first('alias') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Заголовок') !!}
        {!! Form::text('title', null, ['placeholder'=>'Заголовок']) !!}
        @if($errors->has('title'))
            <p class="error">{{ $errors->first('title') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Описание') !!}
        {!! Form::textarea('description', null, ['placeholder'=>'Описание', 'rows' => 3]) !!}
        @if($errors->has('description'))
            <p class="error">{{ $errors->first('description') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Контент') !!}
        {!! Form::textarea('content', null, ['placeholder'=>'Контент', 'id'=>'ckeditor']) !!}
        @if($errors->has('content'))
            <p class="error">{{ $errors->first('content') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::submit('Сохранить изменения', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection