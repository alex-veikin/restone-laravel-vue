@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Вход | ')

@section('content')
    <section>
        <div class="wrapper">
            <h2 class="h2">Вход на сайт</h2>

            <div class="auth-content">
                <div class="item">
                    {!! Form::open(['route'=>'login']) !!}
                    {!! Form::text('login', old('login'), ['placeholder' => 'логин']) !!}
                    @if($errors->has('login'))
                        <p class="error">{{ $errors->first('login') }}</p>
                    @endif
                    {!! Form::password('password', ['placeholder' => 'пароль']) !!}
                    @if($errors->has('password'))
                        <p class="error">{{ $errors->first('password') }}</p>
                    @endif
                    <label>
                        {!! Form::checkbox('remember', old('remember') ? 'checked' : '') !!} Запомнить меня
                    </label>
                    {!! Form::submit('Вход', ['class' => 'submit']) !!}
                    <span>или</span>
                    {!! link_to_route('register', 'Регистрация', [], []) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection
