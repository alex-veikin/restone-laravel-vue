@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Регистрация | ')

@section('content')
    <section>
        <div class="wrapper">
            <h2 class="h2">Регистрация</h2>

            <div class="auth-content">
                <div class="item">
                    {!! Form::open(['route'=>'register']) !!}
                    {!! Form::text('first_name', old('first_name'), ['placeholder' => 'имя']) !!}
                    @if($errors->has('first_name'))
                        <p class="error">{{ $errors->first('first_name') }}</p>
                    @endif
                    {!! Form::text('last_name', old('last_name'), ['placeholder' => 'фамилия']) !!}
                    @if($errors->has('last_name'))
                        <p class="error">{{ $errors->first('last_name') }}</p>
                    @endif
                    {!! Form::text('login', old('login'), ['placeholder' => 'логин']) !!}
                    @if($errors->has('login'))
                        <p class="error">{{ $errors->first('login') }}</p>
                    @endif
                    {!! Form::email('email', old('email'), ['placeholder' => 'email']) !!}
                    @if($errors->has('email'))
                        <p class="error">{{ $errors->first('email') }}</p>
                    @endif
                    {!! Form::password('password', ['placeholder' => 'пароль']) !!}
                    @if($errors->has('password'))
                        <p class="error">{{ $errors->first('password') }}</p>
                    @endif
                    {!! Form::password('password_confirmation', ['placeholder' => 'подтвердите пароль']) !!}
                    {!! Form::submit('Регистрация', ['class'=>'submit']) !!}
                    <span>Уже есть аккаунт?</span>
                    {!! link_to_route('login', 'Вход') !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection
