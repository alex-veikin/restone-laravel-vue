@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', $cuisine->title . ' кухня | ')

@section('content')
    <section class="cuisine">
        <div class="wrapper">
            <h2 class="h2">{{ $cuisine->title }} кухня</h2>

            <div class="description">{{ $cuisine->description }}</div>

            @include('layouts.dishes')
        </div>
    </section>
@endsection