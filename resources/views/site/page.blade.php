@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', $page->title . ' | ')

@section('content')
    <section>
        <div class="wrapper">
            {{ $page->content }}
        </div>
    </section>
@endsection