<section class="cart">
    <div class="wrapper">
        @if(session('cart'))
            <table>
                <tr>
                    <th>№</th>
                    <th>Блюдо</th>
                    <th>Цена</th>
                    <th>Кол-во</th>
                    <th>Сумма</th>
                </tr>
                @foreach(session('cart')->items as $dish)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $dish['item']->title }}</td>
                        <td>{{ $dish['item']->price }}</td>
                        <td class="qty"><a href="">-</a>{{ $dish['qty'] }}<a href="">+</a></td>
                        <td>{{ $dish['price'] }}</td>
                    </tr>
                @endforeach
                <tr class="total">
                    <td colspan="3">Итого:</td>
                    <td>{{ session('cart')->totalQty }}</td>
                    <td>{{ session('cart')->totalPrice }}</td>
                </tr>
            </table>
        @else
            <p>Корзина пуста.</p>
        @endif
    </div>
</section>