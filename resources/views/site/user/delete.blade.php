@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Кабинет пользователя | ')

@section('content')
    <section>
        <div class="wrapper">
            <h2 class="h2">Удаление аккаунта</h2>

            <div class="user-content">
                <div class="item">
                    <div class="alert">Вы уверены, что хотите удалить аккаунт?</div>

                    {!! Form::open(['route'=>'user.delete']) !!}
                    {!! Form::password('password', ['placeholder'=>'текущий пароль']) !!}
                    @if($errors->has('password'))
                        <p class="error">{{ $errors->first('password') }}</p>
                    @endif
                    {!! Form::submit('Удалить аккаунт', ['class'=>'submit']) !!}
                    {!! Form::close() !!}

                    {{ link_to_route('user.index', 'Отмена') }}
                </div>
            </div>
        </div>
    </section>
@endsection